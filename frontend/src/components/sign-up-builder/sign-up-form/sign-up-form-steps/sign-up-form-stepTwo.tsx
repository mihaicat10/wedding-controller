import React, { useState } from "react";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import CURRENCIES from "./../../../../constants/currency";
import {
  Input,
  InputAdornment,
  makeStyles,
  MenuItem,
  Select,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  selectWeddingDate: {
    marginTop: theme.spacing(2),
  },
  selectGuestCount: {
    marginTop: theme.spacing(5),
  },
  selectBudgetTotal: {
    marginTop: theme.spacing(5),
  },
}));

export const SignUpFormStepTwo = () => {
  const [selectedDate, val] = useState(new Date());
  // const [age, setAge] = React.useState("");
  const classes = useStyles();

  function handleDateChange(e: any) {}
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    // setAge(event.target.value as string);
  };

  // const handleChangeAmount = (amount: any) => {
  //   setAmount(amount);
  // };

  return (
    <div>
      <div className={classes.selectWeddingDate}>
        <Typography variant="subtitle1">Choose you wedding date</Typography>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <DatePicker
            fullWidth
            value={selectedDate}
            onChange={(e) => handleDateChange(e)}
          />
        </MuiPickersUtilsProvider>
      </div>
      <div className={classes.selectGuestCount}>
        <Typography variant="subtitle1">Choose your guest count</Typography>
        <Select
          fullWidth={true}
          labelId="guestnumber"
          id="guestnumber"
          value={30}
          required
          onChange={handleChange}
        >
          <MenuItem value={10}>0-20</MenuItem>
          <MenuItem value={20}>20-50</MenuItem>
          <MenuItem value={30}>50-100</MenuItem>
          <MenuItem value={40}>100-150</MenuItem>
          <MenuItem value={40}>150-200</MenuItem>
          <MenuItem value={40}>200-*</MenuItem>
        </Select>
      </div>
      <div className={classes.selectBudgetTotal}>
        <Typography variant="subtitle1">What's your Wedding budget?</Typography>
        <Input
          fullWidth={true}
          id="standard-adornment-amount"
          endAdornment={
            <InputAdornment position="start">{CURRENCIES.RON}</InputAdornment>
          }
        />
      </div>
    </div>
  );
};
