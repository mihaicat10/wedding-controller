import Grid from "@material-ui/core/Grid/Grid";
import React from "react";
import { GCard } from "../../../../common/g-card/g-card";
import { IGCardDataForm } from "../../../../constants/data";

export const SignUpFormStepThree = () => {
  return (
    <div>
      <Grid container>
        {IGCardDataForm.map((data) => {
          return (
            <Grid item xs={4} md={4} key={data.key}>
              <GCard
                img={data.img}
                title={data.title}
                description={data.description}
              />
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
};
