import TextField from "@material-ui/core/TextField/TextField";
import React from "react";

export const SignUpFormStepOne = () => {
  return (
    <div>
      <TextField
        variant="standard"
        margin="normal"
        required
        fullWidth
        id="myname"
        label="My name"
        name="myname"
        autoFocus
      />
      <TextField
        variant="standard"
        margin="normal"
        required
        fullWidth
        id="partnername"
        label="Partner Name"
        name="partnername"
        autoFocus
      />
    </div>
  );
};
