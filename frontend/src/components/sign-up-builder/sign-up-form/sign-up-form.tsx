import {
  Button,
  CssBaseline,
  Grid,
  makeStyles,
  Paper,
} from "@material-ui/core";
import React from "react";
import { SignUpFormStepOne } from "./sign-up-form-steps/sign-up-form-stepOne";
import { SignUpFormStepTwo } from "./sign-up-form-steps/sign-up-form-stepTwo";
import { SignUpFormStepThree } from "./sign-up-form-steps/sign-up-form-stepThree";
import CustomizedSteppers from "../../../common/g-stepper/g-stepper";
import { SignUpError } from "../../../common/errors-view/signup-error";

const useStyles = makeStyles((theme: any) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  dividerForm: {
    margin: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
}));

function renderSteps(step: number) {
  switch (step) {
    case 0:
      return <SignUpFormStepOne />;
    case 1:
      return <SignUpFormStepTwo />;
    case 2:
      return <SignUpFormStepThree />;
    default:
      return <SignUpError />;
  }
}

export const SignUpForm = (props: any) => {
  const [activeStep, setActiveStep] = React.useState(0);
  const classes = useStyles();
  const stepperData = ["Basic info", "Additional info", "Vendors"];

  const handleNext = () => {
    setActiveStep((prevActiveStepSignUpForm) => prevActiveStepSignUpForm + 1);
  };

  const sendToBeHandler = () => {
    console.log(props.children);
  };

  const handleNextBtn = () => {
    if (activeStep < 2) {
      handleNext();
    } else {
      sendToBeHandler();
    }
  };

  return (
    <div>
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <CustomizedSteppers activeStep={activeStep} steps={stepperData} />
            <form className={classes.form} noValidate>
              {renderSteps(activeStep)}
              <Button
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={handleNextBtn}
              >
                Next step
              </Button>
            </form>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};
