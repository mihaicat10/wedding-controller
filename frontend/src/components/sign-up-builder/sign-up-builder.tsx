import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Paper } from "@material-ui/core";
import "./sign-up-builder.css";
import { APP_TITLE } from "../../constants/constants";
import { Link } from "react-router-dom";
import { GLOBAL_ROUTES } from "../../constants/selected-route";
import { Facebook, Shop } from "@material-ui/icons";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      Click “Sign Up” to agree to {APP_TITLE}’s Terms of Service and acknowledge
      that {APP_TITLE}’s Privacy Policy applies to you.
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(20),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: theme.spacing(4),
  },
  signUpTitle: {
    marginBottom: theme.spacing(2),
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  root: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  signUpBtn: {
    // width: "100%",
    // textAlign: "left",
  },
  alreadyHaveAnAccount: {
    marginTop: theme.spacing(5),
  },
  gridStyle: {
    flexGrow: 1,
  },
}));

export default function SignUpBuilder() {
  // TODO 0.1: Align all icons veritcally
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Paper className={classes.paper} variant={"elevation"}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5" className={classes.signUpTitle}>
          Join Wedding Controller
        </Typography>
        <div className={classes.root}>
          <Grid container spacing={2} direction="row" alignItems="stretch">
            <Grid item xs={12}>
              <Button fullWidth variant="outlined" startIcon={<Shop />}>
                Sign up with Google
              </Button>
            </Grid>
            <Grid item xs={12}>
              <Button variant="outlined" fullWidth startIcon={<Facebook />}>
                Sign up with Facebook
              </Button>
            </Grid>

          </Grid>
          <Grid container justify="flex-end">
            <Grid item className={classes.alreadyHaveAnAccount}>
              <Link to={GLOBAL_ROUTES.SIGN_IN}>
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </div>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Paper>
    </Container>
  );
}
