import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {
  Brightness5,
  Done,
  EuroSymbol,
  Flight,
  Home,
  People,
  TextFields,
} from "@material-ui/icons";

import { NAVBAR_DATA } from "../../constants/constants";
import { connect, useDispatch } from "react-redux";
import {
  changeDashboardView,
  getDashboardView,
} from "../../redux/actions/clientDashboardControllerActions";
import { IView } from "../../redux/IState";
import { _viewNameDetector } from "../../services/client-services/utils";

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  tab: {
    minWidth: 20,
  },
});

function CenteredTabs(props: any) {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDashboardView);
  }, [dispatch]);
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    const view: IView = {
      index: newValue,
      additionalData: {},
      viewName: _viewNameDetector(newValue),
    };

    props.changeDashboardView(view);
    setValue(newValue);
  };

  return (
    <Paper className={classes.root}>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        centered
        variant="fullWidth"
      >
        <Tab
          className={classes.tab}
          icon={<Home />}
          label={NAVBAR_DATA.NUNTA_MEA}
        />
        <Tab
          className={classes.tab}
          icon={<EuroSymbol />}
          label={NAVBAR_DATA.BUGET}
        />
        <Tab
          className={classes.tab}
          icon={<Done />}
          label={NAVBAR_DATA.CHECKLIST}
        />
        <Tab
          className={classes.tab}
          icon={<People />}
          label={NAVBAR_DATA.INVITATIONS}
        />
        <Tab
          className={classes.tab}
          icon={<Brightness5 />}
          label={NAVBAR_DATA.TABLE_SIT}
        />
        <Tab
          className={classes.tab}
          icon={<TextFields />}
          label={NAVBAR_DATA.JOURNAL}
        />
        <Tab
          className={classes.tab}
          icon={<Flight />}
          label={NAVBAR_DATA.HONEY_MOON}
        />
      </Tabs>
    </Paper>
  );
}

const mapStateToProps = (state: {
  dashboardController: {
    additionalData: object;
    index: number;
    viewName: string;
  };
}) => ({
  additionalData: state.dashboardController.additionalData,
  index: state.dashboardController.index,
  viewName: state.dashboardController.viewName,
});

export default connect(mapStateToProps, {
  changeDashboardView,
})(CenteredTabs);
