import React from "react";
import { Paper, Theme} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme: Theme) => ({

    paperStyle: {
        marginTop: theme.spacing(2),
        padding: theme.spacing(4),
        maxHeight: '330px',
        textAlign: 'center'
    },
    soon: {
        margin: theme.spacing(5)
    }

}))
const DashboardProviders = () => {
    const classes = useStyles();
    return (
        <div>
            <Paper square={false} className={classes.paperStyle}>
                <div className={classes.soon}>
                    <Typography variant={"h6"}>Furnizorii vor fi adaugati in curand...</Typography>
                </div>
            </Paper>
        </div>
    );
}

export default DashboardProviders;