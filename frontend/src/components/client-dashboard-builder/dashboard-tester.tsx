import { Button } from "@material-ui/core";
import React from "react";
import { connect } from "react-redux";
import { IGuestParams } from "../../redux/IState";

import {
  ColorEnum,
  GuestStatusEnum,
  GuestTypeEnum,
  IChecklist,
  IChecklistStatus,
  IGuest,
  IWeddingOptions,
  SeasonEnum,
  ThemeEnum,
} from "../../redux/IState";
import {
  createChecklist,
  deleteChecklist,
  getChecklists,
  updateChecklist,
} from "../../redux/actions/checklistActions";

import {
  createGuest,
  deleteGuest,
  getGuests,
  updateGuest,
} from "../../redux/actions/guestActions";
import {
  getWeddingOptions,
  updateWeddingOptions,
} from "../../redux/actions/weddingOptionsAction";

const DashboardTester = (props: any) => {
  const handleChecklist = (route: string) => {
    const checklist: IChecklist = {
      title: "Milk way new3",
      description: "buy milk22",
      id: 50,
      status: IChecklistStatus.OPEN,
    };

    if (route === "create") {
      props.createChecklist(checklist);
    }
    console.log("route");
    if (route === "update") {
      console.log("intra");
      props.updateChecklist(checklist);
    }

    if (route === "delete") {
      props.deleteChecklist(checklist);
    }

    if (route === "get") {
      props.getChecklists();
    }
  };
  // export interface IGuest {
  //   id?: string | number;
  //   name: string;
  //   status: GuestStatusEnum;
  //   type: GuestTypeEnum;
  // }
  const guest: IGuest = {
    type: GuestTypeEnum.GUEST_OF_HONOR,
    name: "Florescu Silviu",
    status: GuestStatusEnum.PENDING,
  };

  const weddingOptions: IWeddingOptions = {
    color: ColorEnum.BLACK,
    location: "Bucharest",
    photo_video: "bio bio",
    season: SeasonEnum.AUTUMN,
    theme: ThemeEnum.ALPINE,
  };

  const createGuest = (guest: IGuest) => {
    props.createGuest(guest);
  };

  const deleteGuest = (guest: IGuest) => {
    props.deleteGuest(guest);
  };

  const updateGuest = (guest: IGuest) => {
    props.updateGuest(guest);
  };

  const obj: IGuestParams = {
    limit: 2,
    name: "Flori Pascu",
    status: GuestStatusEnum.PENDING,
    type: 2,
  };
  const getGuests = (obj: IGuestParams) => {
    props.getGuests(obj);
  };

  return (
    <div>
      <h1>Testing proposing</h1>
      <p>Checklist</p>
      <Button variant={"contained"} onClick={(e) => handleChecklist("create")}>
        CREATE CHECKLIST
      </Button>
      <Button variant={"contained"} onClick={(e) => handleChecklist("delete")}>
        DELETE CHECKLIST
      </Button>
      <Button variant={"contained"} onClick={(e) => handleChecklist("update")}>
        UPDATE CHECKLIST
      </Button>
      <Button variant={"contained"} onClick={(e) => handleChecklist("get")}>
        GET CHECKLIST
      </Button>
      <br />
      <p>Guests</p>
      <Button variant={"contained"} onClick={(e) => createGuest(guest)}>
        CREATE Guest
      </Button>
      <Button variant={"contained"} onClick={(e) => deleteGuest(guest)}>
        DELETE Guest
      </Button>
      <Button variant={"contained"} onClick={(e) => updateGuest(guest)}>
        UPDATE Guest
      </Button>
      <Button variant={"contained"} onClick={(e) => getGuests(obj)}>
        GET Guests
      </Button>
      <br />
      <p>Wedding Options</p>
      <Button variant={"contained"} onClick={(e) => props.getWeddingOptions()}>
        GET Wedding Options
      </Button>
      <Button
        variant={"contained"}
        onClick={(e) => props.updateWeddingOptions(weddingOptions)}
      >
        CREATE/UPDATE Wedding Options
      </Button>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  isAuthenticated: state.login.isAuthenticated,
});

export default connect(mapStateToProps, {
  getChecklists,
  createChecklist,
  deleteChecklist,
  updateChecklist,
  createGuest,
  updateGuest,
  deleteGuest,
  getGuests,
  getWeddingOptions,
  updateWeddingOptions,
})(DashboardTester);
