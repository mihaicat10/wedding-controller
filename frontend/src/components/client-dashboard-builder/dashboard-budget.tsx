import { Button, Grid, Paper, Theme, Typography } from "@material-ui/core";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CURRENCIES from "../../constants/currency";
import ProgressBar from "../../common/progress-bar/progress-bar";

const useStyles = makeStyles((theme: Theme) => ({
  textStyle: {
    color: "#9b9b9b",
  },
  root: {
    padding: theme.spacing(1),
    margin: theme.spacing(2),

    alignItems: "center",
  },
  paperStyle: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(4),
    height: "330px",
    [theme.breakpoints.down("md")]: {
      height: "400px",
    },
  },
  textRoot: {
    padding: theme.spacing(4),
    margin: theme.spacing(4),
    textAlign: "center",
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
  },
  ctaBtn: {
    textAlign: "right",
  },
}));

interface IDashboardBudget {
  total_spent_percentage: number;
  budget: number;
}

const DashboardBudget = (props: IDashboardBudget) => {
  const classes = useStyles();
  return (
    <div>
      <Paper square={false} className={classes.paperStyle}>
        <div>
          <Button variant="outlined" color="primary">
            Edit
          </Button>
        </div>
        <Grid container>
          <Grid item xs={6}>
            <div className={classes.textRoot}>
              <Typography variant="h6" className={classes.textStyle}>
                Buget estimativ
              </Typography>
              <Typography variant="caption" className={classes.textStyle}>
                {props.budget} {CURRENCIES.EUR}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={6}>
            <div className={classes.textRoot}>
              <Typography variant="button">TOTAL CHELTUIT DIN BUGET</Typography>
              <ProgressBar value={props.total_spent_percentage} />
            </div>
          </Grid>
        </Grid>
        <div className={classes.ctaBtn}>
          <Button variant="contained" color="primary">
            Gestioneaza buget
          </Button>
        </div>
      </Paper>
    </div>
  );
};

export default DashboardBudget;
