import { Grid } from "@material-ui/core";
import React from "react";
import TextCardSeparator from "../../common/text-card-separator/text-card-separator";
import DashboardBudget from "./dashboard-budget";
import DashboardChecklist from "./dashboard-checklist";
import DashboardDetails from "./dashboard-details";
import DashboardInvites from "./dashboard-invites";
import DashboardInvitesTemplates from "./dashboard-invites-templates";
import DashboardProviders from "./dashboard-providers";
import DashboardCards from "./dashboard-cards";

const DashboardMain = (props: any) => {
  return (
    <div>
      <DashboardCards />
      <TextCardSeparator text={"Detalii nunta"} />
      <DashboardDetails />
      <Grid container spacing={4}>
        <Grid item xs={12} lg={6}>
          <TextCardSeparator text={"Buget"} />
          <DashboardBudget budget={1500} total_spent_percentage={60} />
        </Grid>
        <Grid item xs={12} lg={6}>
          <TextCardSeparator text={"Lista invitati"} />
          <DashboardInvites />
        </Grid>
      </Grid>
      <TextCardSeparator text={"Checklist"} />
      <DashboardChecklist />
      <TextCardSeparator text={"Invitatii online"} />
      <DashboardInvitesTemplates />
      <TextCardSeparator text={"Top furnizori"} />
      <DashboardProviders />
    </div>
  );
};

export default DashboardMain;
