import React from "react";
import { Paper, Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ChecklistTimeline from "../../common/g-timeline/checklist-timeline";
import Button from "@material-ui/core/Button";
import { checklistData } from "../../constants/data";

const useStyles = makeStyles((theme: Theme) => ({
  paperStyle: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(4),
    height: "430px",
    [theme.breakpoints.down("sm")]: {
      height: "480px",
    },
  },
  cta: {
    textAlign: "center",
    marginTop: theme.spacing(2),
  },
}));
const DashboardChecklist = () => {
  const classes = useStyles();
  return (
    <div>
      <Paper square={false} className={classes.paperStyle}>
        <ChecklistTimeline checklistName={checklistData} />
        <div className={classes.cta}>
          <Button color={"primary"} variant={"contained"}>
            Vezi checklistul complet
          </Button>
        </div>
      </Paper>
    </div>
  );
};

export default DashboardChecklist;
