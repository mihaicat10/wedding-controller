export interface IDashboardInputStructure {
  value: string;
  icon: string;
}

export interface IDashboardDetails {
  season: IDashboardInputStructure;
  color: IDashboardInputStructure;
  eventTheme: IDashboardInputStructure;
  location: IDashboardInputStructure;
  photo: IDashboardInputStructure;
}
