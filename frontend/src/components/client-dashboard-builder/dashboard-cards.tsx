import {
  Avatar,
  Button,
  Card,
  CardContent,
  createStyles,
  Grid,
  Hidden,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import React from "react";
import CircularCountdown from "../../common/circular-countdown/circular-countdown";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      marginTop: theme.spacing(2),
    },
    userData: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      height: "225px",
    },
    wedTypography: {
      fontWeight: 1000,
      marginTop: theme.spacing(1),
    },

    avatar: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
    date: {
      color: "#9b9b9b",
      marginBottom: theme.spacing(2),
    },
    image: {
      backgroundImage: "url(https://source.unsplash.com/random)",
      backgroundRepeat: "no-repeat",
      backgroundColor:
        theme.palette.type === "light"
          ? theme.palette.grey[50]
          : theme.palette.grey[900],
      backgroundSize: "cover",
      backgroundPosition: "center",
      [theme.breakpoints.down("xs")]: {
        paddingBottom: theme.spacing(5),
      },
      height: "300px",
    },
  })
);

const DashboardCards = () => {
  const classes = useStyles();
  return (
    <Grid className={classes.root} container spacing={2}>
      <Grid item xs={12} md={6} lg={6}>
        <Card>
          <CardContent>
            <div id="edit-btn">
              <Button variant="outlined" color="primary">
                Edit
              </Button>
            </div>

            <div id="user-data" className={classes.userData}>
              {/* Add here  the first letter of user name */}
              <Avatar className={classes.avatar}>C</Avatar>
              <div>
                <Typography className={classes.wedTypography} variant="h5">
                  Ioana & Catalin
                </Typography>
              </div>
              <Typography className={classes.date} variant="subtitle1">
                July 16, 2020
              </Typography>
              <CircularCountdown />
            </div>
          </CardContent>
        </Card>{" "}
      </Grid>
      <Grid item xs={12} md={6} lg={6}>
        <Hidden xsDown>
          <Card>
            <CardContent className={classes.image}></CardContent>
          </Card>
        </Hidden>
      </Grid>
    </Grid>
  );
};

export default DashboardCards;
