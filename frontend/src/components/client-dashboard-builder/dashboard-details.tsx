import { Button, Grid, Paper, Theme, Typography } from "@material-ui/core";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AcUnitIcon from "@material-ui/icons/AcUnit";
import DashboardDetailsOption from "./dashboard-details-option";
// import { IDashboardDetails } from "./interfaces";

const useStyles = makeStyles((theme: Theme) => ({
  iconTheme: {
    margin: theme.spacing(1),
    fontSize: 40,
  },
  descriptionField: {
    padding: theme.spacing(1),
    color: "#9b9b9b",
  },
  root: {
    padding: theme.spacing(1),
    margin: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  paperStyle: {
    marginTop: theme.spacing(2),
  },
}));

const DashboardDetails = (props: any) => {
  const classes = useStyles();

  return (
    <div>
      <Paper square={false} className={classes.paperStyle}>
        <Grid container>
          <Grid item xs={4} lg={2} className={classes.root}>
            {/* <Typography variant="button">ANOTIMP</Typography> */}
            <DashboardDetailsOption
              title={"ANOTIMP"}
              inputMode={false}
              options={["one", "two"]}
              key={Math.random() * 10}
            />
            <Typography
              className={classes.descriptionField}
              variant="subtitle1"
            >
              IARNA
            </Typography>
            <AcUnitIcon className={classes.iconTheme}></AcUnitIcon>
          </Grid>
          <Grid item xs={4} lg={2} className={classes.root}>
            <DashboardDetailsOption
              title={"CULOARE"}
              inputMode={false}
              options={["one", "two"]}
              key={Math.random() * 10}
            />
            <Typography
              className={classes.descriptionField}
              variant="subtitle1"
            >
              ROZ
            </Typography>
            <AcUnitIcon
              className={classes.iconTheme}
              style={{ backgroundColor: "pink" }}
            ></AcUnitIcon>
          </Grid>
          <Grid item xs={4} lg={2} className={classes.root}>
            <DashboardDetailsOption
              title={"Tematica"}
              inputMode={false}
              options={["one", "two"]}
              key={Math.random() * 10}
            />{" "}
            <Typography
              className={classes.descriptionField}
              variant="subtitle1"
            >
              BEACH
            </Typography>
            <AcUnitIcon className={classes.iconTheme}></AcUnitIcon>
          </Grid>
          <Grid item xs={4} lg={2} className={classes.root}>
            <DashboardDetailsOption
              title={"Locatie"}
              inputMode={true}
              options={["one", "two"]}
              key={Math.random() * 10}
            />
            <Typography
              className={classes.descriptionField}
              variant="subtitle1"
            >
              HADAR
            </Typography>
            <AcUnitIcon className={classes.iconTheme}></AcUnitIcon>
          </Grid>
          <Grid item xs={4} lg={2} className={classes.root}>
            <DashboardDetailsOption
              title={"Locatie"}
              inputMode={true}
              options={["one", "two"]}
              key={Math.random() * 10}
            />
            <Typography
              className={classes.descriptionField}
              variant="subtitle1"
            >
              PHOTOGRAPHY
            </Typography>
            <AcUnitIcon className={classes.iconTheme}></AcUnitIcon>
          </Grid>
          {/*<Grid item xs={4}  lg={2} className={classes.root}>*/}
          {/*    <Typography variant="button">*/}
          {/*        DIVERTISMENT*/}
          {/*    </Typography>*/}
          {/*    <Typography className={classes.descriptionField} variant="subtitle1">*/}
          {/*        THE MONO*/}
          {/*    </Typography>*/}
          {/*    <AcUnitIcon className={classes.iconTheme}>*/}
          {/*    </AcUnitIcon>*/}
          {/*</Grid>*/}
        </Grid>
      </Paper>
    </div>
  );
};

export default DashboardDetails;
