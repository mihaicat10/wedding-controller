import React from "react";
import { Paper, Theme} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ButtonImageCards from "../../common/g-btn-image-cards/btn-image-cards";

const useStyles = makeStyles((theme: Theme) => ({

    iconTheme: {
        margin: theme.spacing(1),
        fontSize: 40
    },
    descriptionField: {
        padding: theme.spacing(1),
        color: "#9b9b9b",
    },
    root: {
        padding: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    paperStyle: {
        marginTop: theme.spacing(2),
        padding: theme.spacing(4),
        height: '410px',
        [theme.breakpoints.down('md')]: {
            height: '600px'
        },
    },
    ctaBtn: {
        textAlign: 'center',
        marginTop: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            marginTop: theme.spacing(0)
            // paddingBottom: theme.spacing(30),
        },

    },


}))
const DashboardInvitesTemplates = () => {
    const classes = useStyles();
    return (
        <div>
            <Paper square={false} className={classes.paperStyle}>
                {/* Add user name here */}
                <Typography variant={"body1"}>Hi Ioana! Have you started thinking about your wedding
                    website?</Typography>
                <Typography variant={"subtitle2"}> Customizable backgrounds, tons of color options, and more. Choose
                    your design!</Typography>
                <Grid container>
                    <Grid item xs={12}  lg={12} className={classes.root}>
                        <ButtonImageCards/>
                    </Grid>
                    {/*<Grid item xs={1} lg={1} className={classes.root}>*/}
                    {/*    <div className={classes.addCtaButton}>*/}
                    {/*        <Fab color="primary" aria-label="add">*/}
                    {/*            <AddIcon/>*/}
                    {/*        </Fab>*/}
                    {/*    </div>*/}
                    {/*</Grid>*/}

                </Grid>
                <div className={classes.ctaBtn}>
                    <Button variant={"contained"} color={"primary"}>Vezi toate modelele</Button>
                </div>
            </Paper>
        </div>
    );
}

export default DashboardInvitesTemplates;