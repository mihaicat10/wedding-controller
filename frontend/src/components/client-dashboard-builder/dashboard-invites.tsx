import React, { useEffect } from "react";
import { Fab, Paper, Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PersonList from "../../common/g-person-list/person-list";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import Grid from "@material-ui/core/Grid";
import { getGuests } from "../../redux/actions/guestActions";
import { connect, useDispatch } from "react-redux";

const useStyles = makeStyles((theme: Theme) => ({
  iconTheme: {
    margin: theme.spacing(1),
    fontSize: 40,
  },
  descriptionField: {
    padding: theme.spacing(1),
    color: "#9b9b9b",
  },
  root: {
    padding: theme.spacing(1),
    margin: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  paperStyle: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(2),
    height: "330px",
    // marginTop: theme.spacing(2),
  },
  ctaBtn: {
    textAlign: "right",
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  addBtnCta: {
    // position: 'absolute',
    textAlign: "right",
  },
}));
const DashboardInvites = (props: any) => {
  const classes = useStyles();

  const dispatch = useDispatch();

  useEffect(() => {
    // dispatch(getGuests());

    // Safe to add dispatch to the dependencies array
  }, [dispatch]);

  //   props.guests

  console.log(props);
  return (
    <div>
      <Paper square={false} className={classes.paperStyle}>
        <Grid container>
          <Grid item xs={10}>
            <PersonList person={props.guests} />
          </Grid>
          <Grid item xs={2}>
            <div className={classes.addBtnCta}>
              <Fab color="primary" aria-label="add">
                <AddIcon />
              </Fab>
            </div>
          </Grid>
        </Grid>
        <div className={classes.ctaBtn}>
          <Button variant={"contained"} color={"primary"}>
            Vezi lista completa
          </Button>
        </div>
      </Paper>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  guests: state.guest.guests,
});

export default connect(mapStateToProps, {
  getGuests,
})(DashboardInvites);
