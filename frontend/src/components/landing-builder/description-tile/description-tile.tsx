import React from "react";
import { Grid, Theme, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { IDescriptionTile } from "../../../interfaces/landing-page-interfaces";

const useStyles = makeStyles((theme: Theme) => ({
  gridStyle: {
    textAlign: "center",
  },
  caption: {
    marginTop: theme.spacing(4),
    color: theme.palette.primary.dark,
  },
  subTitle: {
    marginTop: theme.spacing(4),
    marginLeft: theme.spacing(10),
    marginRight: theme.spacing(10),
  },
  title: {
    marginTop: theme.spacing(0),
  },
}));

export const DescriptionTile = ({
  title,
  body2,
  description,
}: IDescriptionTile) => {
  const classes = useStyles();
  return (
    <div>
      <Grid container className={classes.gridStyle}>
        <Grid item xs={12}>
          <Typography variant={"body2"} className={classes.caption}>
            {body2}
          </Typography>
          <Typography variant={"h4"} className={classes.title}>
            {title}
          </Typography>
          <Typography className={classes.subTitle}>{description}</Typography>
        </Grid>
      </Grid>
    </div>
  );
};
