import React from "react";
import {
  Avatar,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  Paper,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { Link } from "react-router-dom";
import { GLOBAL_ROUTES } from "../../../constants/selected-route";
import * as yup from "yup";
import { useFormik } from "formik";
import { loginUser } from "../../../redux/actions/loginActions";
import { connect } from "react-redux";
import history from '../../../history'

const useStyles = makeStyles((theme) => ({
  buttonStyle: {
    marginLeft: theme.spacing(4),
  },
  title: {
    marginTop: theme.spacing(20),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(4),
  },
  subTitle: {
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(4),
  },
  leftContainer: {
    color: "#ffebee",
  },
  sectionMobile: {
    // display: "flex",
    // [theme.breakpoints.up("lg")]: {
    //   display: "none",
    // },
  },

  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
    [theme.breakpoints.down("xs")]: {
      paddingBottom: theme.spacing(5),
    },
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    grow: 1,
    flexDirection: "column",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const validationSchema = yup.object({
  email: yup
    .string()
    .email("Enter a valid email")
    .required("Email is required"),
  password: yup
    .string()
    .min(8, "Password should be of minimum 8 characters length")
    .required("Password is required"),
});



const Header = (props: any) => {
  const classes = useStyles();

  const formik = useFormik({
    initialValues: {
      email: "mihai.ionescu1094@gmail.com",
      password: "politehnica",
      rememberMe: false,
    },
    validationSchema: validationSchema,
    onSubmit: ({ email, password }) => {
      props.loginUser(email, password);
    },
  });

    const handlePlannerAccess = () => {
        // console.log(props);
        history.push(GLOBAL_ROUTES.SIGN_IN);
        // props.history.push(GLOBAL_ROUTES.SIGN_IN)
    }
  // @ts-ignore
  return (
    <div>
      <div>
        <Grid container className={classes.image}>
          {/*Left side*/}
          <Grid item xs={12} sm={8} md={8} className={classes.leftContainer}>
            <Typography variant="h2" className={classes.title}>
              Nunta ta in orice anotimp.
            </Typography>
            <Typography variant="h5" className={classes.subTitle}>
              The Seasons • plannerul nuntii magice • este sprijinul tau in a
              trai constient minunata calatorie a nuntii tale.
            </Typography>
            <Button
              variant="outlined"
              className={classes.buttonStyle}
              color="primary"
              size="large"
              onClick={() => handlePlannerAccess()}
            >
              Acceseaza plannerul
            </Button>
          </Grid>
          {/*Right side*/}
          <Grid
            item
            xs={12}
            sm={4}
            md={4}
            component={Paper}
            elevation={0}
            square
            className={classes.sectionMobile}
          >
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <form
                className={classes.form}
                onSubmit={(e) => {
                  e.preventDefault();
                  formik.handleSubmit();
                }}
                noValidate
              >
                <TextField
                  id="email"
                  name="email"
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="Email"
                  autoComplete="email"
                  autoFocus
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  error={formik.touched.email && Boolean(formik.errors.email)}
                  helperText={formik.touched.email && formik.errors.email}
                />
                <TextField
                  id="password"
                  name="password"
                  label="Password"
                  type="password"
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  autoComplete="current-password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={formik.touched.password && formik.errors.password}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      value={formik.values.rememberMe}
                      onChange={formik.handleChange}
                      id="rememberMe"
                      name="rememberMe"
                      color="primary"
                    />
                  }
                  label="Remember me"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Sign In
                </Button>
                <Grid container>
                  <Grid item xs>
                    <Typography>Forgot password?</Typography>
                  </Grid>
                  <Grid item>
                    <Link to={GLOBAL_ROUTES.REGISTER}>
                      <Typography>No account yet? Sign up</Typography>
                    </Link>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default connect(null, { loginUser })(Header);
