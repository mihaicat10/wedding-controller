import {Avatar, Grid, Paper, Theme, Typography} from "@material-ui/core";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import NoteIcon from '@material-ui/icons/Note';
import {IDescriptionFeaturesCard} from "../../../interfaces/landing-page-interfaces";

const SvgMapper = (svg: string): JSX.Element => {
    switch (svg) {
        case 'PlaylistAddCheckIcon':
            return (<PlaylistAddCheckIcon/>)
        case 'AttachMoneyIcon':
            return (<AttachMoneyIcon/>)
        case 'NoteIcon':
            return (<NoteIcon/>)
        default:
            return (<svg/>)
    }
}

const useStyles = makeStyles((theme: Theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        margin: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    descriptionField: {
        padding: theme.spacing(2)
    },
}))

export const DescriptionFeaturesCard = ({title, description, icon}: IDescriptionFeaturesCard) => {
    const classes = useStyles();
    return (
        <div>
            {/*xs={12} md={4}*/}
            <Grid>
                <Paper className={classes.paper} square={false}>
                    <Avatar className={classes.avatar}>
                        {SvgMapper(icon)}
                    </Avatar>
                    <Typography component="h1" variant="h6">
                        {title}
                    </Typography>
                    <Typography className={classes.descriptionField} component="h1" variant="subtitle1">
                        {description}
                    </Typography>
                </Paper>
            </Grid>
        </div>
    )
}