import React from "react";
import { Grid, Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { DescriptionFeaturesCard } from "../description-features-card/description-features-card";
import { descriptionFeaturesData } from "../../../constants/data";
import { IDescriptionFeatureData } from "../../../interfaces/landing-page-interfaces";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    marginTop: theme.spacing(3),
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    margin: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
}));

export const DescriptionFeatures = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container className={classes.image} justify="center">
        {descriptionFeaturesData.map(
          (feature: IDescriptionFeatureData, index) => {
            return (
              <DescriptionFeaturesCard
                key={index}
                title={feature.title}
                description={feature.description}
                icon={feature.icon}
              />
            );
          }
        )}
      </Grid>
    </div>
  );
};
