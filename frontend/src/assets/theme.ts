import { createMuiTheme } from "@material-ui/core";

// Custom theme provider
const customTheme = createMuiTheme({
  palette: {
    common: { black: "#000", white: "#fff" },
    background: { paper: "#fff", default: "rgba(250, 250, 250, 1)" },
    primary: {
      light: "rgba(204, 156, 160, 1)",
      main: "rgba(154, 109, 114, 1)",
      dark: "rgba(107, 66, 71, 1)",
      contrastText: "#fff",
    },
    secondary: {
      light: "rgba(255, 205, 209, 1)",
      main: "rgba(204, 156, 160, 1)",
      dark: "rgba(154, 109, 114, 1)",
      contrastText: "#fff",
    },
    error: {
      light: "rgba(229, 115, 115, 1)",
      main: "rgba(175, 68, 72, 1)",
      dark: "rgba(123, 17, 33, 1)",
      contrastText: "#fff",
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(189, 185, 183, 1)",
      disabled: "rgba(189, 185, 183, 1)",
      hint: "rgba(239, 235, 233, 1)",
    },
  },
});

export default customTheme;
