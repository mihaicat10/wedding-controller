export interface IAction {
  payload: any;
  type: string;
}

export interface IActionRequest {
  type: string;
}

export interface IView {
  viewName: string;
  index: number;
  additionalData: object | null;
}

export interface ILogin {
  accessToken: string | null;
  email: string | null;
  isAuthenticated: boolean;
  isAuthenticating: boolean;
  statusText: string | null;
}

export interface IRegister {
  isRegistering: boolean;
  isRegistered: boolean;
  statusText: string | null;
}

export interface IToken {
  email: string;
  exp: number;
  iat: number;
}

export interface IChecklist {
  id?: string | number;
  title: string;
  description: string;
  status?: IChecklistStatus;
  userId?: number;
  user?: any;
}

export interface IGuest {
  id?: string | number;
  createdAt?: Date;
  updatedAt?: Date;
  name: string;
  status: GuestStatusEnum;
  type: GuestTypeEnum;
}

export enum GuestTypeEnum {
  GODFATHER,
  GODMOTHER,
  GUEST_OF_HONOR,
  MAID_OF_HONOR,
  SIMPLE,
}

export enum GuestStatusEnum {
  PENDING,
  DECLINED,
  ACCEPTED,
}

export enum IChecklistStatus {
  OPEN = "OPEN",
  IN_PROGRESS = "IN_PROGRESS",
  DONE = "DONE",
}

export interface IWeddingOptions {
  season: SeasonEnum;
  color: ColorEnum;
  theme: ThemeEnum;
  location: string;
  photo_video: string;
}

export enum SeasonEnum {
  WINTER,
  SPRING,
  SUMMER,
  AUTUMN,
}

export enum ColorEnum {
  RED,
  PINK,
  BLACK,
  BLUE,
}

export enum ThemeEnum {
  BEACH,
  ALPINE,
  COZY,
}

export interface IGuestParams {
  status?: GuestStatusEnum;
  name?: string;
  type?: GuestTypeEnum;
  limit?: number;
}
