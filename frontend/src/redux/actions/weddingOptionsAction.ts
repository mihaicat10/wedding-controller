import { Dispatch } from "redux";
import baseURL from "../../apis/baseURL";
import { WEDDING_OPTIONS } from "../../apis/http-path";
import { _checkHttpStatus } from "../../services/http-service";
import { IAction, IActionRequest, IWeddingOptions } from "../IState";
import {
  GET_WEDDING_OPTIONS_REQUEST,
  GET_WEDDING_OPTIONS_SUCCESS,
  GET_WEDDING_OPTIONS_FAILURE,
  UPDATE_WEDDING_OPTIONS_REQUEST,
  UPDATE_WEDDING_OPTIONS_FAILURE,
  UPDATE_WEDDING_OPTIONS_SUCCESS,
} from "../types/weddingOptionsTypes";

function _getWeddingOptionsRequest(): IActionRequest {
  return {
    type: GET_WEDDING_OPTIONS_REQUEST,
  };
}

function _updateWeddingOptionsRequest(): IActionRequest {
  return {
    type: UPDATE_WEDDING_OPTIONS_REQUEST,
  };
}

function _getWeddingOptionsSuccess(response: any): IAction {
  return {
    type: GET_WEDDING_OPTIONS_SUCCESS,
    payload: {
      weddingOptions: response.data,
    },
  };
}

function _getWeddingOptionsFailure(e: any): IAction {
  return {
    type: GET_WEDDING_OPTIONS_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}

function _updateWeddingOptionsFailure(e: any): IAction {
  return {
    type: UPDATE_WEDDING_OPTIONS_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}

// Create interface for weedingOptions
function _updateWeddingOptionsSuccess(weddingOptions: any): IAction {
  return {
    type: UPDATE_WEDDING_OPTIONS_SUCCESS,
    payload: {
      weddingOptions,
    },
  };
}

export function updateWeddingOptions(weddingOptions: IWeddingOptions) {
  return function (dispatch: Dispatch) {
    const accessToken = localStorage.getItem("accessToken");
    dispatch(_updateWeddingOptionsRequest());
    return baseURL
      .post(WEDDING_OPTIONS.UPDATE, weddingOptions, {
        headers: {
          Authorization: "Bearer " + accessToken,
        },
      })
      .then(_checkHttpStatus)
      .then((response: any) => {
        try {
          dispatch(_updateWeddingOptionsSuccess(response.data));
        } catch (e) {
          dispatch(_updateWeddingOptionsFailure(e));
        }
      })
      .catch((e) => {
        dispatch(_updateWeddingOptionsFailure(e));
      });
  };
}

export function getWeddingOptions() {
  return function (dispatch: Dispatch) {
    const accessToken = localStorage.getItem("accessToken");
    dispatch(_getWeddingOptionsRequest());

    return (
      baseURL
        // add here status & search
        .get(WEDDING_OPTIONS.GET, {
          headers: {
            Authorization: "Bearer " + accessToken,
          },
        })
        .then(_checkHttpStatus)
        .then((response: any) => {
          try {
            dispatch(_getWeddingOptionsSuccess(response));
          } catch (e) {
            dispatch(_getWeddingOptionsFailure(e));
          }
        })
        .catch((e) => {
          dispatch(_getWeddingOptionsFailure(e));
        })
    );
  };
}
