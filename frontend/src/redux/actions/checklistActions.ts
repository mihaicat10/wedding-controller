import { Dispatch } from "redux";
import baseURL from "../../apis/baseURL";
import { CHECKLIST } from "../../apis/http-path";
import { _checkHttpStatus } from "../../services/http-service";
import { IAction, IActionRequest, IChecklist } from "../IState";
import {
  CREATE_CHECKLIST_FAILURE,
  CREATE_CHECKLIST_REQUEST,
  CREATE_CHECKLIST_SUCCESS,
  DELETE_CHECKLIST_FAILURE,
  DELETE_CHECKLIST_REQUEST,
  DELETE_CHECKLIST_SUCCESS,
  GET_CHECKLIST_FAILURE,
  GET_CHECKLIST_REQUEST,
  GET_CHECKLIST_SUCCESS,
  UPDATE_CHECKLIST_FAILURE,
  UPDATE_CHECKLIST_REQUEST,
  UPDATE_CHECKLIST_SUCCESS,
} from "../types/checklistTypes";

function _getChecklistSuccess(response: any): IAction {
  return {
    type: GET_CHECKLIST_SUCCESS,
    payload: {
      checklists: response.data,
    },
  };
}

function _getChecklistFailure(e: any): IAction {
  return {
    type: GET_CHECKLIST_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}

function _getChecklistRequest(): IActionRequest {
  return {
    type: GET_CHECKLIST_REQUEST,
  };
}

function _createChecklistFailure(e: any): IAction {
  return {
    type: CREATE_CHECKLIST_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}
function _createChecklistRequest(checklistPayload: any) {
  return {
    type: CREATE_CHECKLIST_REQUEST,
    checklistPayload,
  };
}

function _createChecklistSuccess(createdChecklist: IChecklist): IAction {
  return {
    type: CREATE_CHECKLIST_SUCCESS,
    payload: {
      checklist: createdChecklist,
    },
  };
}

function _deleteChecklistFailure(e: any): IAction {
  return {
    type: DELETE_CHECKLIST_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}
function _deleteChecklistRequest(): IActionRequest {
  return {
    type: DELETE_CHECKLIST_REQUEST,
  };
}
function _deleteChecklistSuccess(response: any, id: number): IAction {
  return {
    type: DELETE_CHECKLIST_SUCCESS,
    payload: {
      status: response,
      id,
    },
  };
}

function _updateChecklistFailure(e: any): IAction {
  return {
    type: UPDATE_CHECKLIST_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}
function _updateChecklistRequest(): IActionRequest {
  return {
    type: UPDATE_CHECKLIST_REQUEST,
  };
}
function _updateChecklistSuccess(checklist: IChecklist): IAction {
  return {
    type: UPDATE_CHECKLIST_SUCCESS,
    payload: {
      checklist: checklist,
    },
  };
}

export function getChecklists(status = null, search = null) {
  return function (dispatch: Dispatch) {
    const accessToken = localStorage.getItem("accessToken");
    dispatch(_getChecklistRequest());

    return (
      baseURL
        // add here status & search
        .get(CHECKLIST.GET, {
          headers: {
            Authorization: "Bearer " + accessToken,
          },
        })
        .then(_checkHttpStatus)
        .then((response: any) => {
          try {
            dispatch(_getChecklistSuccess(response));
          } catch (e) {
            dispatch(_getChecklistFailure(e));
          }
        })
        .catch((e) => {
          dispatch(_getChecklistFailure(e));
        })
    );
  };
}

export function createChecklist(checklist: IChecklist) {
  return function (dispatch: Dispatch) {
    const accessToken = localStorage.getItem("accessToken");
    dispatch(_createChecklistRequest(checklist));
    return baseURL
      .post(CHECKLIST.CREATE, checklist, {
        headers: {
          Authorization: "Bearer " + accessToken,
        },
      })
      .then(_checkHttpStatus)
      .then((response: any) => {
        try {
          dispatch(_createChecklistSuccess(response.data));
        } catch (e) {
          dispatch(_createChecklistFailure(e));
        }
      })
      .catch((e) => {
        dispatch(_createChecklistFailure(e));
      });
  };
}

export function deleteChecklist(checklist: IChecklist) {
  return function (dispatch: Dispatch) {
    const id = checklist.id;
    const accessToken = localStorage.getItem("accessToken");
    dispatch(_deleteChecklistRequest());
    return baseURL
      .delete(CHECKLIST.DELETE + id, {
        headers: {
          Authorization: "Bearer " + accessToken,
        },
      })
      .then(_checkHttpStatus)
      .then((response: any) => {
        try {
          dispatch(
            _deleteChecklistSuccess(response.data.result, response.data.id)
          );
        } catch (e) {
          dispatch(_deleteChecklistFailure(e));
        }
      })
      .catch((e) => {
        dispatch(_deleteChecklistFailure(e));
      });
  };
}

export function updateChecklist(checklist: IChecklist) {
  return function (dispatch: Dispatch) {
    const id = checklist.id;
    const status = checklist.status;
    const accessToken = localStorage.getItem("accessToken");

    // check if status is correct
    dispatch(_updateChecklistRequest());
    return baseURL
      .patch(CHECKLIST.UPDATE + id, status, {
        headers: {
          Authorization: "Bearer " + accessToken,
        },
      })
      .then(_checkHttpStatus)
      .then((response: any) => {
        try {
          dispatch(_updateChecklistSuccess(response.data));
        } catch (e) {
          dispatch(_updateChecklistFailure(e));
        }
      })
      .catch((e) => {
        dispatch(_updateChecklistFailure(e));
      });
  };
}
