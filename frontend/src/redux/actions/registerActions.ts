import {IAction} from './../IState';
import {_checkHttpIsCreated} from "../../services/http-service";
import {REGISTER_USER_FAILURE, REGISTER_USER_REQUEST, REGISTER_USER_SUCCESS} from "../types/registerTypes";
import baseURL from "../../apis/baseURL";
import {Dispatch} from "redux";
import {AUTH} from "../../apis/http-path";
import History from "../../history";
import {HTTP_CODES} from "../../constants/constants";
import {GLOBAL_ROUTES} from "../../constants/selected-route";


export const registerUser = (email: string, password: string) => (
    dispatch: Dispatch
) => {
    dispatch(_registerUserRequest())
    baseURL
        .post(AUTH.REGISTER, {email, password})
        .then(_checkHttpIsCreated)
        .then((response: any) => {
            try {
                dispatch(_registerUserSuccess());
            } catch (e) {
                dispatch(
                    _registerUserFailure({
                        response: {
                            status: 403,
                            statusText: "InvalidToken",
                        },
                    })
                );
            }
        })
        .catch((error: any) => {
            dispatch(_registerUserFailure(error));
        });
};


function _registerUserSuccess() {
    History.push(GLOBAL_ROUTES.LANDING_PAGE)
    return {
        type: REGISTER_USER_SUCCESS,
    };
}


function _registerUserFailure(error: any): IAction {
    return {
        type: REGISTER_USER_FAILURE,
        payload: {
            status: error?.response?.status ? error.response.status : "503",
            statusText: error?.response?.statusText ? error.response.statusText : HTTP_CODES["503"],
        },
    };
}

function _registerUserRequest() {
    return {
        type: REGISTER_USER_REQUEST,
    };
}

