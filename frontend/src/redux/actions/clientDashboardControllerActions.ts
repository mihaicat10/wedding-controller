import { IView } from "../IState";
import {
  CHANGE_DASHBOARD_VIEW,
  GET_DASHBOARD_VIEW,
} from "../types/clientDashboardTypes";

export function changeDashboardView(view: IView) {
  return {
    type: CHANGE_DASHBOARD_VIEW,
    payload: view,
  };
}

export function getDashboardView() {
  return {
    type: GET_DASHBOARD_VIEW,
  };
}
