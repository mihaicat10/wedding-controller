import { Dispatch } from "redux";
import baseURL from "../../apis/baseURL";
import { GUEST } from "../../apis/http-path";
import { _checkHttpStatus } from "../../services/http-service";
import { IGuestParams } from "../IState";
import {
  GuestStatusEnum,
  GuestTypeEnum,
  IAction,
  IActionRequest,
  IGuest,
} from "../IState";
import {
  CREATE_GUEST_FAILURE,
  CREATE_GUEST_REQUEST,
  CREATE_GUEST_SUCCESS,
  DELETE_GUEST_FAILURE,
  DELETE_GUEST_REQUEST,
  DELETE_GUEST_SUCCESS,
  GET_GUEST_FAILURE,
  GET_GUEST_REQUEST,
  GET_GUEST_SUCCESS,
  UPDATE_GUEST_FAILURE,
  UPDATE_GUEST_REQUEST,
  UPDATE_GUEST_SUCCESS,
} from "../types/guestTypes";

function _getGuestRequest(): IActionRequest {
  return {
    type: GET_GUEST_REQUEST,
  };
}

function _getGuestSuccess(response: any): IAction {
  return {
    type: GET_GUEST_SUCCESS,
    payload: {
      guests: response.data,
    },
  };
}

function _getGuestFailure(e: any): IAction {
  return {
    type: GET_GUEST_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}

function _createGuestFailure(e: any): IAction {
  return {
    type: CREATE_GUEST_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}

function _createGuestRequest(guestPayload: IGuest): any {
  return {
    type: CREATE_GUEST_REQUEST,
    guestPayload,
  };
}

function _createGuestSuccess(createdGuest: IGuest): IAction {
  return {
    type: CREATE_GUEST_SUCCESS,
    payload: {
      guest: createdGuest,
    },
  };
}

function _deleteGuestFailure(e: any): IAction {
  return {
    type: DELETE_GUEST_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}

function _deleteGuestRequest(): IActionRequest {
  return {
    type: DELETE_GUEST_REQUEST,
  };
}
function _deleteGuestSuccess(response: any, id: number): IAction {
  return {
    type: DELETE_GUEST_SUCCESS,
    payload: {
      status: response,
      id,
    },
  };
}

function _updateGuestFailure(e: any): IAction {
  return {
    type: UPDATE_GUEST_FAILURE,
    payload: {
      status: e?.response?.data?.message ? e?.response.data?.message : "503",
      statusText: e?.response?.data?.statusCode
        ? e?.response?.data?.statusCode
        : "503",
    },
  };
}
function _updateGuestRequest(): IActionRequest {
  return {
    type: UPDATE_GUEST_REQUEST,
  };
}
function _updateGuestSuccess(guest: IGuest): IAction {
  return {
    type: UPDATE_GUEST_SUCCESS,
    payload: {
      guest: guest,
    },
  };
}

export function getGuests(options: IGuestParams) {
  return function (dispatch: Dispatch) {
    const accessToken = localStorage.getItem("accessToken");
    dispatch(_getGuestRequest());
    const { limit, name, status, type } = options;
    return (
      baseURL
        // add here status & search
        .get(GUEST.GET, {
          params: { limit, name, status, type },
          headers: {
            Authorization: "Bearer " + accessToken,
          },
        })
        .then(_checkHttpStatus)
        .then((response: any) => {
          try {
            dispatch(_getGuestSuccess(response));
          } catch (e) {
            dispatch(_getGuestFailure(e));
          }
        })
        .catch((e) => {
          dispatch(_getGuestFailure(e));
        })
    );
  };
}

// TOOD: Rethink this and ofc move outside of this file
function _checkBoundaries(guest: IGuest) {
  if (
    Object.values(GuestStatusEnum).includes(guest.status) &&
    Object.values(GuestTypeEnum).includes(guest.type)
  ) {
    return true;
  }
  return false;
}

export function createGuest(guest: IGuest) {
  return function (dispatch: Dispatch) {
    const accessToken = localStorage.getItem("accessToken");
    dispatch(_createGuestRequest(guest));
    return baseURL
      .post(GUEST.CREATE, guest, {
        headers: {
          Authorization: "Bearer " + accessToken,
        },
      })
      .then(_checkHttpStatus)
      .then((response: any) => {
        try {
          dispatch(_createGuestSuccess(response.data));
        } catch (e) {
          dispatch(_createGuestFailure(e));
        }
      })
      .catch((e) => {
        dispatch(_createGuestFailure(e));
      });
  };
}
export function deleteGuest(guest: IGuest) {
  return function (dispatch: Dispatch) {
    const id = guest.id;
    const accessToken = localStorage.getItem("accessToken");
    dispatch(_deleteGuestRequest());
    return baseURL
      .delete(GUEST.DELETE + id, {
        headers: {
          Authorization: "Bearer " + accessToken,
        },
      })
      .then(_checkHttpStatus)
      .then((response: any) => {
        try {
          dispatch(_deleteGuestSuccess(response.data.result, response.data.id));
        } catch (e) {
          dispatch(_deleteGuestFailure(e));
        }
      })
      .catch((e) => {
        dispatch(_deleteGuestFailure(e));
      });
  };
}
export function updateGuest(guest: IGuest) {
  return function (dispatch: Dispatch) {
    // const status = invite.status;
    const accessToken = localStorage.getItem("accessToken");

    // check if status is correct
    dispatch(_updateGuestRequest());
    return baseURL
      .patch(GUEST.UPDATE, guest, {
        headers: {
          Authorization: "Bearer " + accessToken,
        },
      })
      .then(_checkHttpStatus)
      .then((response: any) => {
        try {
          dispatch(_updateGuestSuccess(response.data));
        } catch (e) {
          dispatch(_updateGuestFailure(e));
        }
      })
      .catch((e) => {
        dispatch(_updateGuestFailure(e));
      });
  };
}
