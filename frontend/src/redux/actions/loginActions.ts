import { IAction } from "../IState";
import baseURL from "../../apis/baseURL";

import { Dispatch } from "redux";
import {
  FETCH_PROTECTED_DATA_REQUEST,
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  RECEIVE_PROTECTED_DATA,
  LOGIN_USER_REQUEST,
} from "../types/userTypes";
import { _checkHttpStatus } from "../../services/http-service";
import { AUTH } from "../../apis/http-path";
import { HTTP_CODES } from "../../constants/constants";
import { CLIENT_ROUTES, GLOBAL_ROUTES } from "../../constants/selected-route";
import history from "../../history";

function _loginUserSuccess(accessToken: string): IAction {
  localStorage.setItem("accessToken", accessToken);
  return {
    type: LOGIN_USER_SUCCESS,
    payload: {
      accessToken: accessToken,
    },
  };
}

function _loginUserFailure(error: any): IAction {
  localStorage.removeItem("accessToken");
  return {
    type: LOGIN_USER_FAILURE,
    payload: {
      status: error?.response?.status ? error.response.status : "503",
      statusText: error?.response?.statusText
        ? error.response.statusText
        : HTTP_CODES["503"],
    },
  };
}

function _loginUserRequest() {
  return {
    type: LOGIN_USER_REQUEST,
  };
}

export function logout() {
  localStorage.removeItem("accessToken");
  history.push(GLOBAL_ROUTES.LANDING_PAGE);
  return {
    type: LOGOUT_USER,
  };
}

export function receiveProtectedData(data: any) {
  return {
    type: RECEIVE_PROTECTED_DATA,
    payload: {
      data,
    },
  };
}

export function fetchProtectedDataRequest() {
  return {
    type: FETCH_PROTECTED_DATA_REQUEST,
  };
}

// this is just an example
export function fetchProtectedData(accessToken: string) {
  return (dispatch: Dispatch, state: any) => {
    dispatch(fetchProtectedDataRequest());
    return baseURL
      .get("http://localhost:3001/getSomethingWithToken", {
        headers: { Authorization: "Bearer " + accessToken },
      })
      .then(_checkHttpStatus)
      .then((response) => {
        dispatch(receiveProtectedData(response.data));
      })
      .catch((error) => {
        if (error.response.status === 401) {
          dispatch(_loginUserFailure(error));
          history.push("/");
        }
      });
  };
}

export function loginUser(email: string, password: string) {
  return function (dispatch: Dispatch) {
    dispatch(_loginUserRequest());

    return baseURL
      .post(AUTH.SIGN_IN, { password, email })
      .then(_checkHttpStatus)
      .then((response: any) => {
        try {
          dispatch(_loginUserSuccess(response.data.accessToken));
          history.push(CLIENT_ROUTES.DASHBOARD);
        } catch (e) {
          dispatch(
            _loginUserFailure({
              response: {
                status: 403,
                statusText: "Invalid token",
              },
            })
          );
        }
      })
      .catch((err) => {
        dispatch(_loginUserFailure(err));
      });
  };
}

// INFO: axios with token on header

// axios.get('https://example.com/getSomething', {
//  headers: {
//    Authorization: 'Bearer ' + token //the token is a variable which holds the token
//  }
// })

// export const fetchUser = (userInfo: any) => (dispatch: Dispatch) => {
//   baseURL
//     .post("http://localhost:3001/auth/signin", userInfo)
//     .then((data: any) => {
//       console.log("this ***", data);
//       const accessToken = data.data.accessToken;
//       const user = data.data.user;
//       localStorage.setItem("accessToken", accessToken);
//       dispatch(setUser(user));
//     })
//     .catch((err) => {
//       localStorage.removeItem("accessToken");
//       dispatch(setUserErr(err));
//     });
// };

// export const autoLogin = () => (dispatch: Dispatch) => {
//     fetch(`http://localhost:3001/${AUTO_LOGIN}`, {
//         headers: {
//             "Content-Type": "application/json",
//             "Accept": "application/json",
//             "Authorization": `Bearer ${localStorage.getItem("token")}`
//         }
//     })
//         .then(res => res.json())
//         .then((data) => {
//             // data sent back will in the format of
//             // {
//             //     user: {},
//             //.    token: "aaaaa.bbbbb.bbbbb"
//             // }
//             localStorage.setItem("token", data.token)
//             dispatch(setUser(data.user))
//         })
// }
