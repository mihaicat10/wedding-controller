import { ColorEnum, SeasonEnum, ThemeEnum } from "../IState";
import {
  GET_WEDDING_OPTIONS_REQUEST,
  UPDATE_WEDDING_OPTIONS_REQUEST,
  GET_WEDDING_OPTIONS_SUCCESS,
  GET_WEDDING_OPTIONS_FAILURE,
  UPDATE_WEDDING_OPTIONS_FAILURE,
  UPDATE_WEDDING_OPTIONS_SUCCESS,
} from "../types/weddingOptionsTypes";

const initialState = {
  isLoading: false,
  statusText: null,
  season: SeasonEnum,
  color: ColorEnum,
  theme: ThemeEnum,
  location: "",
  photo_video: "",
};

const weddingOptionsReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_WEDDING_OPTIONS_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    case UPDATE_WEDDING_OPTIONS_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    case GET_WEDDING_OPTIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        season: action.payload.weddingOptions.season,
        color: action.payload.weddingOptions.color,
        theme: action.payload.weddingOptions.theme,
        location: action.payload.weddingOptions.location,
        photo_video: action.payload.weddingOptions.photovideo,
      };
    case GET_WEDDING_OPTIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        statusText: `Cannot GET wedding options: ${action.payload.status} ${action.payload.statusText}`,
      };
    case UPDATE_WEDDING_OPTIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        statusText: `Cannot UPDATE wedding options: ${action.payload.status} ${action.payload.statusText}`,
      };
    case UPDATE_WEDDING_OPTIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        season: action.payload.weddingOptions.season,
        color: action.payload.weddingOptions.color,
        theme: action.payload.weddingOptions.theme,
        location: action.payload.weddingOptions.location,
        photo_video: action.payload.weddingOptions.photovideo,
      };
    default:
      return initialState;
  }
};

export default weddingOptionsReducer;
