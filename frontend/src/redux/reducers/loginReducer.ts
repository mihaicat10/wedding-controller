import {
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  LOGIN_USER_REQUEST,
} from "./../types/userTypes";
import jwtDecode from "jwt-decode";
import { ILogin, IToken } from "../IState";

const initialState: ILogin = {
  accessToken: null,
  email: null,
  isAuthenticated: false,
  isAuthenticating: false,
  statusText: null,
};

const loginReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case LOGIN_USER_REQUEST:
      return {
        ...state,
        isAuthenticating: true,
        statusText: null,
      };
    case LOGIN_USER_SUCCESS:
      const token: IToken = jwtDecode(action.payload.accessToken);
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: true,
        accessToken: action.payload.token,
        email: token.email,
        statusText: "You have been successfully logged in.",
      };
    case LOGIN_USER_FAILURE:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: false,
        accessToken: null,
        email: null,
        statusText: `Authentication Error: ${action.payload.status} ${action.payload.statusText}`,
      };
    case LOGOUT_USER:
      return {
        ...state,
        isAuthenticated: false,
        accessToken: null,
        email: null,
        statusText: "You have been successfully logged out.",
      };
    default:
      return initialState;
  }
};
export default loginReducer;
