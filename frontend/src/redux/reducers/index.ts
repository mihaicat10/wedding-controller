import { combineReducers } from "redux";
import loginReducer from "./loginReducer";
import registerReducer from "./registerReducer";
import checklistReducer from "./checklistReducer";
import guestReducer from "./guestReducer";
import weddingOptionsReducer from "./weddingOptionsReducer";
import clientDashboardControllerReducer from "./clientDashboardControllerReducer";

const rootReducer = combineReducers({
  login: loginReducer,
  register: registerReducer,
  checklist: checklistReducer,
  guest: guestReducer,
  dashboardController: clientDashboardControllerReducer,
  weddingOption: weddingOptionsReducer,
});

export default rootReducer;
