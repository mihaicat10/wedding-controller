import {REGISTER_USER_FAILURE, REGISTER_USER_REQUEST, REGISTER_USER_SUCCESS} from "../types/registerTypes";
import {IRegister} from "../IState";

const initialState: IRegister = {
    isRegistering: false,
    isRegistered: false,
    statusText: null
}

const registerReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case REGISTER_USER_REQUEST:
            return {
                ...state,
                isRegistering: true,
            }
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                isRegistered: true,
                isRegistering: false,
                isRegistered_statusText: "You have been successfully registered",
            }
        case REGISTER_USER_FAILURE:
            return {
                ...state,
                isRegistered: false,
                isRegistering: false,
                isRegistered_statusText: `Registration Error: ${action.payload.status} ${action.payload.statusText}`
            }
        default:
            return initialState
    }
}

export default registerReducer;