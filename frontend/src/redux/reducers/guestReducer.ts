import { IGuest } from "../IState";
import {
  CREATE_GUEST_FAILURE,
  CREATE_GUEST_REQUEST,
  CREATE_GUEST_SUCCESS,
  DELETE_GUEST_FAILURE,
  DELETE_GUEST_REQUEST,
  DELETE_GUEST_SUCCESS,
  GET_GUEST_FAILURE,
  GET_GUEST_REQUEST,
  GET_GUEST_SUCCESS,
  UPDATE_GUEST_FAILURE,
  UPDATE_GUEST_REQUEST,
  UPDATE_GUEST_SUCCESS,
} from "../types/guestTypes";

const initialState = {
  isLoading: false,
  statusText: null,
  guests: [],
  guest: {},
};

const guestReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_GUEST_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    case GET_GUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        guests: action.payload.guests,
      };
    case GET_GUEST_FAILURE:
      return {
        ...state,
        isLoading: false,
        statusText: `Cannot get invites: ${action.payload.status} ${action.payload.statusText}`,
        guests: [],
      };
    case CREATE_GUEST_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    // Check this login
    case CREATE_GUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        guest: action.payload.guest,
        guests: [...state.guests, action.payload.guest],
      };
    case CREATE_GUEST_FAILURE:
      return {
        ...state,
        isLoading: false,
        guest: null,
        statusText: `Cannot create guest: ${action.payload.status} ${action.payload.statusText}`,
      };
    case DELETE_GUEST_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    case DELETE_GUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        guests: state.guests.filter(
          (guest: IGuest) => guest.id !== action.payload.id
        ),
        statusText: action.payload.status.affected === 1 ? "success" : "fail",
      };
    case DELETE_GUEST_FAILURE:
      return {
        ...state,
        isLoading: false,
        statusText: `Cannot delete guest: ${action.payload.status} ${action.payload.statusText}`,
      };
    case UPDATE_GUEST_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    case UPDATE_GUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        guests: state.guests.map((guest: IGuest) => {
          if (guest.id === action.payload.guest.id) {
            guest.name = action.payload.guest.name;
            guest.type = action.payload.guest.type;
            guest.status = action.payload.guest.status;
          }
          return guest;
        }),
      };
    case UPDATE_GUEST_FAILURE:
      return {
        ...state,
        isLoading: false,
        statusText: `Cannot update guest: ${action.payload.status} ${action.payload.statusText}`,
      };

    default:
      return initialState;
  }
};

export default guestReducer;
