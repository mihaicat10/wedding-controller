import {
  CREATE_CHECKLIST_FAILURE,
  CREATE_CHECKLIST_REQUEST,
  CREATE_CHECKLIST_SUCCESS,
  DELETE_CHECKLIST_FAILURE,
  DELETE_CHECKLIST_REQUEST,
  DELETE_CHECKLIST_SUCCESS,
  GET_CHECKLIST_FAILURE,
  GET_CHECKLIST_REQUEST,
  GET_CHECKLIST_SUCCESS,
  UPDATE_CHECKLIST_FAILURE,
  UPDATE_CHECKLIST_REQUEST,
  UPDATE_CHECKLIST_SUCCESS,
} from "../types/checklistTypes";

import { IChecklist } from "./../IState";

const initialState = {
  isLoading: false,
  statusText: null,
  checklists: [],
  checklist: {},
};

const checklistReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_CHECKLIST_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    case GET_CHECKLIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        checklists: action.payload.checklists,
      };
    case GET_CHECKLIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        statusText: `Cannot get checklists: ${action.payload.status} ${action.payload.statusText}`,
        checklists: [],
      };
    case CREATE_CHECKLIST_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    // Check this login
    case CREATE_CHECKLIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        checklist: action.payload.checklist,
        checklists: [...state.checklists, action.payload.checklist],
      };
    case CREATE_CHECKLIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        checklist: null,
        statusText: `Cannot create checklist: ${action.payload.status} ${action.payload.statusText}`,
      };
    case DELETE_CHECKLIST_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    case DELETE_CHECKLIST_SUCCESS:
      // need a new way to delete checklist/guest/etc from redux
      return {
        ...state,
        checklists: state.checklists.filter(
          (checklist: IChecklist) => checklist.id !== action.payload.id
        ),
        statusText: action.payload.status.affected === 1 ? "success" : "fail",
      };
    case DELETE_CHECKLIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        statusText: `Cannot delete checklist: ${action.payload.status} ${action.payload.statusText}`,
      };
    case UPDATE_CHECKLIST_REQUEST:
      return {
        ...state,
        isLoading: true,
        statusText: null,
      };
    // Check this.
    case UPDATE_CHECKLIST_SUCCESS:
      return {
        ...state,
        checklists: state.checklists.map((checklist: IChecklist) => {
          if (checklist.id === action.payload.id) {
            checklist.description = action.payload.description;
            checklist.status = action.payload.status;
            checklist.title = action.payload.title;
          }
          return checklist;
        }),
      };

    case UPDATE_CHECKLIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        statusText: `Cannot update checklist: ${action.payload.status} ${action.payload.statusText}`,
      };

    default:
      return initialState;
  }
};

export default checklistReducer;
