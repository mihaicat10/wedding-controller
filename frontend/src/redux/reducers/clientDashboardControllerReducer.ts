import { CLIENT_VIEWS_OPTION } from "../../constants/constants";
import { IView } from "../IState";
import {
  CHANGE_DASHBOARD_VIEW,
  GET_DASHBOARD_VIEW,
} from "../types/clientDashboardTypes";

const initialState: IView = {
  additionalData: {},
  index: 0,
  viewName: CLIENT_VIEWS_OPTION.DASHBOARD,
};

const clientDashboardControllerReducer = (
  state = initialState,
  action: any
) => {
  switch (action.type) {
    case CHANGE_DASHBOARD_VIEW:
      return {
        ...state,
        index: action.payload.index,
        viewName: action.payload.viewName,
      };
    case GET_DASHBOARD_VIEW:
      return {
        ...state,
      };
    default:
      return initialState;
  }
};

export default clientDashboardControllerReducer;
