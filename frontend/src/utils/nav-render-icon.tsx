import { Avatar } from "@material-ui/core";
import {
  Brightness5,
  Done,
  EuroSymbol,
  Flight,
  Home,
  People,
  TextFields,
} from "@material-ui/icons";
import React from "react";

export const navRenderIcon = (num: number) => {
  switch (num) {
    case 0:
      return <Home />;
    case 1:
      return <EuroSymbol />;
    case 2:
      return <Done />;
    case 3:
      return <People />;
    case 4:
      return <Brightness5 />;
    case 5:
      return <TextFields />;
    case 6:
      return <Flight />;
    default:
      return <Avatar />;
  }
};
