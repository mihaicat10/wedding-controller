import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Container, Grid, Typography } from "@material-ui/core";
import { footers } from "../../constants/data";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) => ({
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
}));

// Futures:
//     A static template with example of: checklist, guestlist, invite list
// Company:
//     Team
// Contact us
// Location
// Legal:
//     Privacy policy
// Terms of use
// Resources:
//     Articles from internet *  or even social media promo (currently 0)

export const Footer = () => {
  const classes = useStyles();
  return (
    <div>
      <Container maxWidth="md" component="footer" className={classes.footer}>
        <Grid container spacing={4} justify="space-evenly">
          {footers.map((footer) => (
            <Grid item xs={6} sm={3} key={footer.title}>
              <Typography variant="h6" color="textPrimary" gutterBottom>
                {footer.title}
              </Typography>
              <ul>
                {footer.description.map((item: string) => (
                  <li key={item}>
                    {/*Replace this with Link URL*/}
                    <Link to={item}>{item}</Link>
                  </li>
                ))}
              </ul>
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
};
