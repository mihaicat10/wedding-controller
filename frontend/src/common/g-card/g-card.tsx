import ButtonBase from "@material-ui/core/ButtonBase/ButtonBase";
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import CardMedia from "@material-ui/core/CardMedia/CardMedia";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography/Typography";
import React, { useState } from "react";
import { IGCard } from "../../interfaces/common-interfaces";

const useStyles = makeStyles((theme) => ({
  cardAction: {
    display: "block",
    textAlign: "initial",
  },
  card: {
    margin: theme.spacing(1),
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    height: 130,

    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },

  hoverEffect: {
    opacity: "85%",
    borderBottom: "1px groove #1C6EA4",
  },
  clickEffect: {
    backgroundColor: "#d81b60",
    color: "#ffffff",
  },
}));

export const GCard = ({ description, img, title }: IGCard) => {
  const [hovered, setHovered] = useState(false);
  const [clicked, setClicked] = useState(false);
  const toggleClick = () => setClicked(!clicked);
  const toggleHover = () => setHovered(!hovered);

  const classes = useStyles();

  return (
    <div>
      <Card
        className={`${classes.card} ${hovered ? classes.hoverEffect : ""}  
        } `}
        onMouseEnter={toggleHover}
        onMouseLeave={toggleHover}
      >
        <ButtonBase
          className={`${classes.cardAction}`}
          onClick={() => toggleClick()}
        >
          {/* ${clicked ? classes.clickEffect : "" */}
          <CardMedia className={classes.image} />
          <CardContent className={`${clicked ? classes.clickEffect : ""}`}>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {description}
            </Typography>
          </CardContent>
        </ButtonBase>
      </Card>
    </div>
  );
};
