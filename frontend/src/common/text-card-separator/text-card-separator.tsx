import {makeStyles} from "@material-ui/core/styles";
import {Theme} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import React from "react";

interface ITextCardSeparator {
    text: string;
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        marginTop: theme.spacing(2)
    }
}))


// TODO: Make this more beautiful and util
const TextCardSeparator = (props: ITextCardSeparator) => {
    const classes = useStyles();
    return (
        <Typography variant={"h5"} className={classes.root}>
            {props.text}
        </Typography>
    )
}

export default TextCardSeparator