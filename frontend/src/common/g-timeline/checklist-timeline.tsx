import React from "react";
import Timeline from "@material-ui/lab/Timeline";
import TimelineItem from "@material-ui/lab/TimelineItem";
import TimelineSeparator from "@material-ui/lab/TimelineSeparator";
import TimelineConnector from "@material-ui/lab/TimelineConnector";
import TimelineContent from "@material-ui/lab/TimelineContent";
import TimelineDot from "@material-ui/lab/TimelineDot";
import Typography from "@material-ui/core/Typography";
import TimelineOppositeContent from "@material-ui/lab/TimelineOppositeContent";

interface IChecklistName {
  checklist_name: string;
  checklist_date: string;
}

interface IChecklistTimeline {
  checklistName: IChecklistName[];
}

export default function ChecklistTimeline(checklist: IChecklistTimeline) {
  return (
    <Timeline align="alternate">
      {checklist.checklistName.map((checklist: IChecklistName, index) => {
        return (
          <TimelineItem key={index}>
            <TimelineOppositeContent>
              <Typography color="textSecondary">
                {checklist.checklist_date}
              </Typography>
            </TimelineOppositeContent>
            <TimelineSeparator>
              <TimelineDot />
              <TimelineConnector />
            </TimelineSeparator>
            <TimelineContent>
              <Typography>{checklist.checklist_name}</Typography>
            </TimelineContent>
          </TimelineItem>
        );
      })}
    </Timeline>
  );
}
