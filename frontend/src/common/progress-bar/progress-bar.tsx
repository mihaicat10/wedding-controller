import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import theme from "../../assets/theme";

interface IProgressBar {
    value: number;
}

const useStyles = makeStyles({
    root: {
        width: '100%',
        padding: theme.spacing(2),

    },
    linearBarStyle: {
        height: '6px'
    }
});

export default function ProgressBar(props: IProgressBar) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <LinearProgress className={classes.linearBarStyle} variant="determinate" value={props.value} />
        </div>
    );
}