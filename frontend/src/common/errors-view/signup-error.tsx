import { makeStyles, Typography } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(10),
    marginLeft: theme.spacing(2),
  },
}));

export const SignUpError = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h6">404 :(</Typography>
      <Typography>
        Maybe the step you are looking for has been removed :(
      </Typography>
    </div>
  );
};
