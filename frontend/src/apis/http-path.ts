export const AUTH = {
  SIGN_IN: "/auth/signin",
  REGISTER: "/auth/signup",
};

export const CHECKLIST = {
  GET: "/checklist",
  CREATE: "/checklist",
  DELETE: "/checklist/",
  UPDATE: "/checklist/",
};

export const INVITATION = {
  GET: "/invitation",
  CREATE: "/invitation",
  DELETE: "/invitation/",
  UPDATE: "/invitation/",
};

export const WEDDING_OPTIONS = {
  GET: "/weddingoptions",
  UPDATE: "/weddingoptions",
};

export const GUEST = {
  GET: "/guest",
  CREATE: "/guest",
  DELETE: "/guest/",
  UPDATE: "/guest/",
};
