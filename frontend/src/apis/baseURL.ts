import axios from "axios";
const LOCAL_HOST = "http://localhost:3001";


export default axios.create({
  baseURL: LOCAL_HOST,
});

// const instance = axios.create({
//   withCredentials: true,
//   baseURL: API_SERVER,
// });
