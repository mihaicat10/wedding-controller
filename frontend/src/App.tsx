import React from "react";
import "./App.css";
import { Redirect, Route, Router } from "react-router-dom";
import { CLIENT_ROUTES, GLOBAL_ROUTES } from "./constants/selected-route";
import LandingPage from "./pages/global/landing-page/lading-page";
import { SignUp } from "./pages/global/sign-up/sign-up";
import { SignUpForm } from "./components/sign-up-builder/sign-up-form/sign-up-form";
import SignIn from "./pages/global/sign-in/sign-in";
import PrivateRoute from "./services/privateRoute";
import history from "./history";
import Register from "./pages/global/sign-up/register";
import ClientDashboardPlatform from "./pages/client/client-dashboard/client-dashboard-platform";
import { checkIfLogged } from "./services/auth-service";

function App() {
  const isLoggedIn = checkIfLogged();

  return (
    <div>
      <Router history={history}>
        {/*{*/}
        {/*    !userReducer.loggedIn ? <h1>Sign Up or Login!</h1> : <h1>Welcome, {userReducer.user.username}</h1>*/}
        {/*}*/}
        {/*<Route*/}
        {/*  path={SelectedRoute.LANDING_PAGE}*/}
        {/*  exact*/}
        {/*  component={LandingPage}*/}
        {/*/>*/}
        <Route path={GLOBAL_ROUTES.SIGN_UP} exact component={SignUp} />
        {/*Exclude Signup-mail from routing itself*/}
        <Route
          path={GLOBAL_ROUTES.LANDING_PAGE}
          exact
          component={LandingPage}
        />
        <Route path={GLOBAL_ROUTES.REGISTER} exact component={Register} />
        <Route path={GLOBAL_ROUTES.SIGN_UP_FORM} exact component={SignUpForm} />
        <Route path={GLOBAL_ROUTES.SIGN_IN} exact component={SignIn} />

        <PrivateRoute
          path={CLIENT_ROUTES.DASHBOARD}
          component={ClientDashboardPlatform}
        />

        {isLoggedIn ? (
          <Redirect to={CLIENT_ROUTES.DASHBOARD} />
        ) : (
          <Redirect to={GLOBAL_ROUTES.LANDING_PAGE} />
        )}
      </Router>
    </div>
  );
}

export default App;
