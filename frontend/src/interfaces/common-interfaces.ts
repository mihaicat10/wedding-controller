export interface IGCard {
  img: string;
  title: string;
  description: string;
  key?: number;
}

export interface IGStepper { 
  steps: string[];
  activeStep: number;
}