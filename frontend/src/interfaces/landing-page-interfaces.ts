export interface IDescriptionTile {
  body2: string;
  title: string;
  description: string;
}

export interface IDescriptionFeaturesCard {
  title: string;
  description: string;
  icon: string;
}

export interface IDescriptionFeatureData {
  title: string;
  description: string;
  icon: string;
}

export interface ICardFeatureData {
  body2: string;
  title: string;
  description: string;
}

export interface IFooter {
  title: string;
  description: Array<string>;
}
