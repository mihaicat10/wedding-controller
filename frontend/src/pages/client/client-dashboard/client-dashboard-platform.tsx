import React, { useEffect } from "react";
import clsx from "clsx";
import {
  makeStyles,
  useTheme,
  Theme,
  createStyles,
} from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MailIcon from "@material-ui/icons/Mail";
import { Badge, Hidden, Menu, MenuItem } from "@material-ui/core";
import NotificationsIcon from "@material-ui/icons/Notifications";
import MoreIcon from "@material-ui/icons/MoreVert";
import AccountCircle from "@material-ui/icons/AccountCircle";
import logo from "../../../assets/images/logo.png"; // with import
import CenteredTabs from "../../../components/client-dashboard-builder/dashboard-tabs";
import { navRenderIcon } from "../../../utils/nav-render-icon";
import { NAVBAR_TITLES } from "../../../constants/constants";
import { logout } from "../../../redux/actions/loginActions";
import { connect, useDispatch, useSelector } from "react-redux";
import { RenderMobileMenu } from "./mobile-menu";
import DashboardTester from "../../../components/client-dashboard-builder/dashboard-tester";
import { getDashboardView } from "../../../redux/actions/clientDashboardControllerActions";
import { getChecklists } from "../../../redux/actions/checklistActions";
import DashboardMain from "../../../components/client-dashboard-builder/dashboard-main";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    grow: {
      flexGrow: 1,
    },
    drawerHeader: {
      display: "flex",
      alignItems: "center",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: "flex-end",
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
    },
    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
    sectionDesktop: {
      display: "none",
      [theme.breakpoints.up("sm")]: {
        display: "flex",
      },
    },
    sectionMobile: {
      display: "flex",
      [theme.breakpoints.up("sm")]: {
        display: "none",
      },
    },

    featureTitle: {
      marginTop: theme.spacing(2),
    },
  })
);

function ClientDashboardPlaform(props: any) {
  const dispatch = useDispatch();
  const classes = useStyles();

  // how to call an redux action
  useEffect(() => {
    dispatch(getChecklists());
    // Safe to add dispatch to the dependencies array
  }, [dispatch]);

  useEffect(() => {}, [props.index]);

  // how to use an redux data
  const checklists = useSelector((state: any) => state.checklist);

  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [
    mobileMoreAnchorEl,
    setMobileMoreAnchorEl,
  ] = React.useState<null | HTMLElement>(null);

  const menuId = "primary-search-account-menu";
  const mobileMenuId = "primary-search-account-menu-mobile";

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };
  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const myAccountHandler = () => {
    return null;
  };
  const logoutHandler = () => {
    props.logout();
  };

  const handleMenuOption = (e: any, option: string) => {
    switch (option) {
      case "profile":
        setAnchorEl(null);
        setMobileMoreAnchorEl(null);
        break;
      case "myaccount":
        myAccountHandler();
        break;
      case "logout":
        logoutHandler();
        break;
      default:
        setAnchorEl(null);
        setMobileMoreAnchorEl(null);
        break;
    }
  };

  const renderMenu = (
    <Menu
      className={classes.sectionDesktop}
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuOption}
    >
      <MenuItem onClick={(e) => handleMenuOption(e, "profile")}>
        Profile
      </MenuItem>
      <MenuItem onClick={(e) => handleMenuOption(e, "myaccount")}>
        My account
      </MenuItem>
      <MenuItem onClick={(e) => handleMenuOption(e, "logout")}>
        Log out
      </MenuItem>
    </Menu>
  );

  const handleView = (i: number) => {
    switch (i) {
      case 0:
        return <DashboardMain />;
      case 1:
        return <div>Budget View</div>;
      case 2:
        return <div>Checklist</div>;
      case 3:
        return <div>Invitations</div>;
      case 4:
        return <div>Table sit</div>;
      case 5:
        return <div>Journal</div>;
      case 6:
        return <div>Honey moon</div>;
      default:
        return null;
    }
  };
  const handleTest = (e: any) => {
    console.log(e);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      {/* Refactor this */}
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <Hidden smUp>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
          </Hidden>
          <img src={logo} alt="logo" width="130px" height="60px" />

          <div className={classes.grow} />

          <div className={classes.sectionDesktop}>
            <IconButton aria-label="show 4 new mails" color="inherit">
              <Badge badgeContent={4} color="secondary">
                <MailIcon />
              </Badge>
            </IconButton>
            <IconButton aria-label="show 17 new notifications" color="inherit">
              <Badge badgeContent={17} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
        {renderMenu}
        <RenderMobileMenu
          mobileMoreAnchorEl={mobileMoreAnchorEl}
          mobileMenuId={mobileMenuId}
          isMobileMenuOpen={isMobileMenuOpen}
          handleMobileMenuClose={handleMobileMenuClose}
          handleMenuOption={handleMenuOption}
        />
      </AppBar>
      {/*End of refactor*/}
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        {/* Mobile View only */}
        <List>
          {NAVBAR_TITLES.map((text, index) => (
            <ListItem button key={text} onClick={(e: any) => handleTest(index)}>
              <ListItemIcon>{navRenderIcon(index)}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <Divider />
      </Drawer>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />
        <div>
          <Hidden xsDown>
            <CenteredTabs />
          </Hidden>
          {/* {props.index === 0 ? <DashboardMain /> : <div></div>} */}
          {handleView(props.index)}

          <DashboardTester />
        </div>
      </main>
    </div>
  );
}

const mapStateToProps = (state: {
  login: { isAuthenticated: boolean };
  dashboardController: {
    additionalData: object;
    index: number;
    path: string;
    viewName: string;
  };
}) => ({
  isAuthenticated: state.login.isAuthenticated,
  additionalData: state.dashboardController.additionalData,
  index: state.dashboardController.index,
  path: state.dashboardController.path,
  viewName: state.dashboardController.viewName,
});

// @ts-ignore
export default connect(mapStateToProps, {
  logout,
  getDashboardView,
})(ClientDashboardPlaform);
