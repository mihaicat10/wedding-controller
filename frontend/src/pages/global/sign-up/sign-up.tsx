import React from "react";
import SignUpBuilder from "../../../components/sign-up-builder/sign-up-builder";
import {Container} from "@material-ui/core";


export const SignUp = () => {
    return (
        <Container maxWidth="xl"><SignUpBuilder/></Container>
    )
}