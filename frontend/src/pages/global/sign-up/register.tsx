import {
  Button,
  CssBaseline,
  Grid,
  makeStyles,
  Paper,
} from "@material-ui/core";
import React from "react";
import TextField from "@material-ui/core/TextField/TextField";
import { useFormik } from "formik";
import * as yup from "yup";
import { connect } from "react-redux";
import { registerUser } from "../../../redux/actions/registerActions";

const useStyles = makeStyles((theme: any) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },

  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  dividerForm: {
    margin: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
}));

const validationSchema = yup.object({
  email: yup
    .string()
    .email("Enter a valid email")
    .required("Email is required"),
  password: yup
    .string()
    .min(8, "Password should be of minimum 8 characters length")
    .required("Password is required"),
});

const Register = (props: any) => {
  const classes = useStyles();

  const handleBackButton = () => {
    props.history.goBack();
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: ({ email, password }) => {
      props.registerUser(email, password);
      // TODO: what to do if username/pwd is not good?
    },
  });

  return (
    <div>
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <form
              className={classes.form}
              onSubmit={formik.handleSubmit}
              noValidate
            >
              <TextField
                id="email"
                name="email"
                label="Email"
                value={formik.values.email}
                onChange={formik.handleChange}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
                variant="standard"
                margin="normal"
                required
                fullWidth
                autoFocus
              />
              <TextField
                id="password"
                name="password"
                label="Password"
                type="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
                variant="standard"
                margin="normal"
                required
                fullWidth
              />{" "}
              <Button
                fullWidth
                variant="contained"
                color="primary"
                type="submit"
                className={classes.submit}
              >
                Sign up
              </Button>
              {/* TODO: Please think for a better button for this operation */}
              <Button
                fullWidth
                variant="contained"
                color="secondary"
                className={classes.submit}
                onClick={() => handleBackButton()}
              >
                Go back
              </Button>
            </form>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};
export default connect(null, { registerUser })(Register);
