import {
    Button,
    Checkbox,
    Container,
    FormControlLabel,
    Grid,
    Paper,
    TextField,
    Typography,
} from "@material-ui/core";
import {Link} from "react-router-dom";
import {GLOBAL_ROUTES} from "../../../constants/selected-route";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import {APP_TITLE} from "../../../constants/constants";
import "./sign-in.css";
import FacebookIcon from "@material-ui/icons/Facebook";
import * as yup from "yup";
import {useFormik} from "formik";
import {loginUser} from "../../../redux/actions/loginActions";
import {connect} from "react-redux";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: "15vh",
        display: "flex",
        flexDirection: "column",
        padding: theme.spacing(4),
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(2),
    },
    submit: {
        margin: theme.spacing(2, 0, 2),
    },
    socialMediaBtns: {
        marginTop: theme.spacing(2),
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 5,
    },
    socialMediaBtn: {
        width: 140,
    },
    textwicon: {
        marginLeft: theme.spacing(1),
    },
}));


const validationSchema = yup.object({
    email: yup
        .string()
        .email('Enter a valid email')
        .required('Email is required'),
    password: yup
        .string()
        .min(8, 'Password should be of minimum 8 characters length')
        .required('Password is required'),
});

 const SignIn = (props: any) => {
    const classes = useStyles();

    const formik = useFormik({
        initialValues: {
            email: 'mihai.ionescu1094@gmail.com',
            password: 'politehnica',
            rememberMe: false
        },
        validationSchema: validationSchema,
        onSubmit: ({email, password}) => {
            props.loginUser(email,password);
        },

    });


    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <Grid item xs={12} sm={12} md={12}>
                <Paper className={classes.paper} variant={"elevation"}>
                    <div>
                        <Typography component="h1" variant="h6">
                            Sign in to your account
                        </Typography>
                        <Typography variant="subtitle1">
                            Don't have an {APP_TITLE} account?
                        </Typography>
                        <Link to={GLOBAL_ROUTES.SIGN_UP}>Create one</Link>
                    </div>
                    <form
                        // role="form"
                        className={classes.form}
                        onSubmit={(e) => {
                            e.preventDefault();
                            formik.handleSubmit();
                        }}>

                        <TextField
                            id="email"
                            name="email"
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            label="Email"
                            autoComplete="email"
                            autoFocus
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            error={formik.touched.email && Boolean(formik.errors.email)}
                            helperText={formik.touched.email && formik.errors.email}
                        />
                        <TextField
                            id="password"
                            name="password"
                            label="Password"
                            type="password"
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            autoComplete="current-password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            error={formik.touched.password && Boolean(formik.errors.password)}
                            helperText={formik.touched.password && formik.errors.password}
                        />
                        <FormControlLabel
                            control={
                                <Checkbox value={formik.values.rememberMe}
                                          onChange={formik.handleChange}
                                          id="rememberMe"
                                          name="rememberMe"
                                          color="primary"/>}
                            label="Remember me"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign In
                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <Typography>
                                    <Link to={"#"}> Forgot password?</Link>
                                </Typography>
                            </Grid>
                        </Grid>
                        <div className="separator">OR USE</div>
                        <div className={classes.socialMediaBtns}>
                            <Button
                                className={classes.socialMediaBtn}
                                variant="contained"
                                color="secondary"
                            >
                                Google
                            </Button>
                            <Button
                                className={classes.socialMediaBtn}
                                variant="contained"
                                color="primary"
                            >
                                <FacebookIcon/>
                                <span className={classes.textwicon}>Facebook</span>
                            </Button>
                        </div>
                    </form>
                </Paper>
            </Grid>
        </Container>
    );
};

 export default connect(null, {loginUser})(SignIn);
