import React from "react";
import Navbar from "../../../common/navbar/navbar";
import Header from "../../../components/landing-builder/header/header";
import { DescriptionTile } from "../../../components/landing-builder/description-tile/description-tile";
import { DescriptionFeatures } from "../../../components/landing-builder/description-features/description-features";
import { cardFeatureData as textData } from "../../../constants/data";
import { Footer } from "../../../common/footer/footer";

class LandingPage extends React.Component<any, any> {
  render() {
    return (
      <div>
        <Navbar />
        <Header />
        <DescriptionTile
          description={textData[0].description}
          title={textData[0].title}
          body2={textData[0].body2}
        />
        <DescriptionFeatures />
        <DescriptionTile
          description={textData[1].description}
          title={textData[1].title}
          body2={textData[1].body2}
        />
        <Footer />
      </div>
    );
  }
}

export default LandingPage;
