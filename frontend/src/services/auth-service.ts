import jwtDecode from "jwt-decode";

export function checkIfLogged(): boolean {
    let token: string | null = localStorage.getItem('accessToken');

    if(token) {
        // @ts-ignore
        let tokenExpiration = jwtDecode(token).exp;
        let dateNow = new Date();

        if(tokenExpiration < dateNow.getTime()/1000) {
            return false;
        } else {
            return true;
        }
    }
    return false;
}