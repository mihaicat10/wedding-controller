export function _checkHttpStatus(response: any) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    var error: any = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}

export function _checkHttpIsCreated(response: any) {
    if(response.status === 201 || 200) {
        return response;
    } else {
        const error: any = new Error(response.statusText);
        error.response = response;
        throw new Error(error);
    }
}