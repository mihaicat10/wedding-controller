import { CLIENT_ROUTES } from "./../../constants/selected-route";
export const routeHandler = (history: any, direction: string) => {
  const route = _machingRoutes(direction);
  console.log(route);

  switch (direction) {
    case CLIENT_ROUTES.CHECK_LIST:
      history.push(CLIENT_ROUTES.CHECK_LIST);
      break;
    case CLIENT_ROUTES.BUDGET_MANAGER:
      history.push(CLIENT_ROUTES.BUDGET_MANAGER);
      break;
    case CLIENT_ROUTES.GUEST_LIST:
      history.push(CLIENT_ROUTES.GUEST_LIST);
      break;
    case CLIENT_ROUTES.NOTES:
      history.push(CLIENT_ROUTES.NOTES);
      break;
    default:
      break;
  }
};

const _machingRoutes = (direction: string) => {
  switch (direction) {
    case "Checklist":
      return CLIENT_ROUTES.CHECK_LIST;
    case "Notes":
      return CLIENT_ROUTES.NOTES;
    case "Guest List":
      return CLIENT_ROUTES.GUEST_LIST;
    case "Budget Manager":
      return CLIENT_ROUTES.BUDGET_MANAGER;
    default:
      return CLIENT_ROUTES.DASHBOARD;
  }
};
