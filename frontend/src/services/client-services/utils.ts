import DashboardMain from "../../components/client-dashboard-builder/dashboard-main";
import { CLIENT_VIEWS_OPTION } from "../../constants/constants";

export function _viewNameDetector(i: number) {
  switch (i) {
    case 0:
      return CLIENT_VIEWS_OPTION.DASHBOARD;
    case 1:
      return CLIENT_VIEWS_OPTION.BUDGET;
    case 2:
      return CLIENT_VIEWS_OPTION.CHECKLIST;
    case 3:
      return CLIENT_VIEWS_OPTION.INVITATION;
    case 4:
      return CLIENT_VIEWS_OPTION.TABLE_SIT;
    case 5:
      return CLIENT_VIEWS_OPTION.JOURNAL;
    case 6:
      return CLIENT_VIEWS_OPTION.HONEY_MOON;
    default:
      return "";
  }
}
