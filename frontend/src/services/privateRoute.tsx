import React, { useEffect, useState} from 'react';
import { Route, Redirect } from 'react-router-dom'
import { useSelector } from "react-redux";
import jwtDecode from "jwt-decode";


// @ts-ignore
const PrivateRoute = ({ component: Component, ...rest }) => {
    // Implement TS

    const loginState = useSelector((state: any) => state.login)
    const [isAuthenticated, setIsAuthenticated] = useState(null)
    useEffect(() => {
        let token: string | null = localStorage.getItem('accessToken')
        if(token){
            // @ts-ignore
            let tokenExpiration = jwtDecode(token).exp;
            let dateNow = new Date();

            if(tokenExpiration < dateNow.getTime()/1000){
                // @ts-ignore
                setIsAuthenticated(false)
            }else{
                // @ts-ignore
                setIsAuthenticated(true)
            }
        } else {
            // @ts-ignore
            setIsAuthenticated(false)
        }
        // eslint-disable-next-line
    }, [loginState])

    if(isAuthenticated === null){
        return <div/>
    }

    return (
        <Route {...rest} render={props =>
            !isAuthenticated ? (
                <Redirect to='/'/>
            ) : (
                <Component {...props} />
            )
        }
        />
    );
};

export default PrivateRoute;