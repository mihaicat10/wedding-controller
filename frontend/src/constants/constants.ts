export const APP_TITLE = "THE SEASONS";

export const HTTP_CODES = {
  503: "Service Unavailable",
};

export const NAVBAR_DATA = {
  NUNTA_MEA: "NUNTA MEA",
  BUGET: "BUGET",
  CHECKLIST: "CHECKLIST",
  INVITATIONS: "INVITATII",
  TABLE_SIT: "ASEZARE MASA",
  JOURNAL: "JURNAL",
  HONEY_MOON: "LUNA DE MIERE",
};

export const NAVBAR_TITLES = [
  NAVBAR_DATA.NUNTA_MEA,
  NAVBAR_DATA.BUGET,
  NAVBAR_DATA.CHECKLIST,
  NAVBAR_DATA.INVITATIONS,
  NAVBAR_DATA.TABLE_SIT,
  NAVBAR_DATA.JOURNAL,
  NAVBAR_DATA.HONEY_MOON,
];

export const CLIENT_VIEWS_OPTION = {
  DASHBOARD: "DASHBOARD",
  BUDGET: "BUDGET",
  CHECKLIST: "CHECKLIST",
  INVITATION: "INVITATION",
  TABLE_SIT: "TABLE_SIT",
  JOURNAL: "JOURNAL",
  HONEY_MOON: "HONEY_MOON",
};
