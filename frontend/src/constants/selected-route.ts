export const GLOBAL_ROUTES = {
  LANDING_PAGE: "/",
  SIGN_UP: "/signup",
  SIGN_IN: "/signin",
  SIGN_UP_FORM: "/signup-form",
  PROTECTED_ROUTE_PROTOTYPE: "/protected",
  REGISTER: "/register",
};

export const CLIENT_ROUTES = {
  DASHBOARD: "/client/dashboard",
  CHECK_LIST: "/client/checklist",
  NOTES: "/client/mynotes",
  GUEST_LIST: "/client/guest-list",
  BUDGET_MANAGER: "/client/budget-manager",
};
