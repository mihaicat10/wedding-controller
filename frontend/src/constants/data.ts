import {IGCard} from "../interfaces/common-interfaces";
import {
    ICardFeatureData,
    IDescriptionFeatureData,
    IFooter,
} from "../interfaces/landing-page-interfaces";

export const personList = [
    {
        id: 1,
        fullName: "Florina Stan",
        description: "Domnisoara de onoare",
        imageSrc: "/static/images/avatar/1.jpg"
    },
    {
        id: 2,
        fullName: "Andreea Eftimie",
        description: "Nasa",
        imageSrc: "/static/images/avatar/2.jpg"
    },
    {
        id: 3,
        fullName: "Roxana Elena Stefan",
        description: "Domnisoara de onoare",
        imageSrc: "/static/images/avatar/3.jpg"
    }
]

export const checklistData = [
    {
        checklist_name: "Cumpara inel",
        checklist_date: "14 mai 2021"
    },
    {
        checklist_name: "Cumpara pantaloni",
        checklist_date: "12 iulie 2021"
    },
    {
        checklist_name: "Cumpara camasa",
        checklist_date: "1 sep 2021"
    },
    {
        checklist_name: "Cumpara cravata",
        checklist_date: "5 oct 2021"
    },


]

export const cardFeatureData: ICardFeatureData[] = [
    {
        body2: "Povestea THE SEASONS",
        title: "Povestea noastra",
        description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
            "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris" +
            " nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit " +
            "in voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
            "Excepteur sint occaecat cupidatat non proident, " +
            "sunt in culpa qui officia deserunt mollit anim id est laborum.",
    },
    {
        body2: "DE CE THE SEASONS?",
        title: "Pentru ca noi te ajutam sa traiesti constient.\n",
        description:
            "The Seasons iti propune sa ai totul la un singur clcik, oriunde," +
            "pe telefon, tableta, leptop. Ai access la plannerul tau de oriunde si iti " +
            "poti nota ideile imediat cum iti trec prin minte. Te poti dedica complet nuntii tale iar mecanismele noastre inteligente iti vor simplifica munca si te rog ajuta. Spre exemplu iti oferim un calculator bugetar care iti calculeaza automat cheltuielile facute pana intr-un anumit moment dat. Astfel vei stii clar cand ai depasit o anumita suma si iti poti revizui aspectele legate de bugetul nuntii la orice pas.",
    },
];

export const descriptionFeaturesData: IDescriptionFeatureData[] = [
    {
        title: "Checklist-ul magic",
        description:
            "Vei urmari indeaprope timeline-ul nuntii tale si tot ceea ce trebuie sa faci pana in ziua cea mare.\n" +
            "\n",
        icon: "PlaylistAddCheckIcon",
    },
    {
        title: "Planificarea bugetului\n",
        description:
            "Vei putea urmari toate cheltuielile si vei stii in orice moment care este bilantul cheltuielilor nuntii tale.\n" +
            "\n",
        icon: "PlaylistAddCheckIcon",
    },
    {
        title: "Website-ul nuntii tale\n",
        description:
            "Nunta ta va avea propriul ei spatiu online • pe care il vei putea impartasii cu invitatii oricand vei dori.\n" +
            "\n",
        icon: "PlaylistAddCheckIcon",
    },
    {title: "one", description: "two", icon: "AttachMoneyIcon"},
    {title: "one", description: "two", icon: "PlaylistAddCheckIcon"},
    {title: "one", description: "two", icon: "NoteIcon"},
];

export const footers: IFooter[] = [
    {
        title: "Company",
        description: ["Team", "History", "Contact us", "Locations"],
    },
    {
        title: "Features",
        description: [
            "Cool stuff",
            "Random feature",
            "Team feature",
            "Developer stuff",
            "Another one",
        ],
    },
    {
        title: "Resources",
        description: [
            "Resource",
            "Resource name",
            "Another resource",
            "Final resource",
        ],
    },
    {
        title: "Legal",
        description: ["Privacy policy", "Terms of use"],
    },
];

export const IGCardDataForm: IGCard[] = [
    {
        title: "Restaurant",
        description: "A cool restaurant so you can take a launch",
        img: "restaurant.jpg",
        key: 1,
    },
    {
        title: "DJ",
        description: "A cool DJ so you can take a song",
        img: "dj.jpg",
        key: 2,
    },
    {
        title: "Flowers",
        description: "A cool flower shop so you can take a launch",
        img: "restaurant.jpg",
        key: 3,
    },
    {
        title: "Photograph",
        description: "A cool photograph  so you can take a foto",
        img: "restaurant.jpg",
        key: 4,
    },
    {
        title: "MoneyShop",
        description: "A cool moneyShop so you can take a foto",
        img: "restaurant.jpg",
        key: 5,
    },
];

export const clientDashboardActions: string[] = [
    "Checklist",
    "Notes",
    "Guest List",
    "Budget Manager",
];

export const clientDashboardVendors: string[] = [
    "Music",
    "Flowers",
    "Rings",
    "Photo",
];
