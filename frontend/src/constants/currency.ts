const CURRENCIES = {
  RON: 'RON',
  DOL: '$',
  EUR: '€',
  GBP: '£'
}

export default CURRENCIES;