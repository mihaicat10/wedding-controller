import { Module } from '@nestjs/common';
import { typeOrmConfig } from './config/typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './modules/auth/auth.module';
import { ChecklistModule } from './modules/checklist/checklist.module';
import { UserModule } from './modules/user/user.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GuestModule } from './modules/guest/guest.module';
import { WeddingOptionsModule } from './modules/wedding-options/wedding-options.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    AuthModule,
    ChecklistModule,
    UserModule,
    GuestModule,
    WeddingOptionsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
