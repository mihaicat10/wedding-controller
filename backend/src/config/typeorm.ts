import {TypeOrmModuleOptions} from "@nestjs/typeorm";
import * as config from 'config';

const db_env = config.get('db');

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: db_env.type,
    host: process.env.RDS_HOSTNAME || db_env.host,
    port: process.env.RDS_PORT || db_env.port,
    username: process.env.RDS_USERNAME ||  db_env.username,
    password: process.env.RDS_PASSWORD || db_env.password,
    database: process.env.DB_NAME ||  db_env.database,
    entities: [__dirname + '/../**/*.entity.{js,ts}'],
    synchronize: process.env.TYPEORM_SYNC || db_env.synchronize // not recommended in production
}