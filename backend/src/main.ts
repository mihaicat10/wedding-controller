import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import * as config from 'config';
import * as helmet from 'helmet';
import * as compression from 'compression';
import * as rateLimit from 'express-rate-limit';
import { setupSwagger } from './config/api-swagger';

// populate data with db Data
// continue refactor in dashboard if needs
// * finish redux: invite & weddingOptions

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  const serverConfig = config.get('server');
  const logger = new Logger('bootstrap');

  // secure app by setting various HTTP headers.
  app.use(helmet());

  // enable gzip compression.
  app.use(compression());

  // protect app from brute-force attacks
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );

  if (!process.env.NODE_ENV) {
    logger.log('Application is running in DEVELOPMENT mode');
    setupSwagger(app);
    app.enableCors();
  }

  // PORT=3001 npm run start - script for running with env
  const port = process.env.PORT || serverConfig.port;
  await app.listen(port);
  logger.log('Application listening on port ' + port);
}

bootstrap();
