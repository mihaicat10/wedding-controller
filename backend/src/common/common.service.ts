import {Request} from "express";
import {UserEntity} from "../modules/user/user.entity";
import {NotFoundException} from "@nestjs/common";

export const handleUserRequest = (req: Request): UserEntity => {
    // Maybe it feels like redudant but it's cool to have double check sometimes :)
    // @ts-ignore
    const user: UserEntity = req.user;
    if (!user) {
        throw new NotFoundException("User not found.")
    }
    return user;
}