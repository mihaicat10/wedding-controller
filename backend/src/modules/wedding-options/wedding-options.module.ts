import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

import { AuthModule } from '../auth/auth.module';
import { UserRepository } from '../user/user.repository';
import { WeddingOptionsRepository } from './wedding-options.repository';
import { WeddingOptionsController } from './wedding-options.controller';
import { WeddingOptionsService } from './wedding-options.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([WeddingOptionsRepository, UserRepository]),
    AuthModule,
  ],
  controllers: [WeddingOptionsController],
  providers: [WeddingOptionsService],
})
export class WeddingOptionsModule {}
