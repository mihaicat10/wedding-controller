import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { AbstractEntity } from '../shared/abstract.entity';
import { UserEntity } from '../user/user.entity';
import { ColorEnum, SeasonEnum, ThemeEnum } from './dto/wedding-options.enums';

@Entity({ name: 'weddingoptions' })
export class WeddingOptionsEntity extends AbstractEntity {
  @Column()
  season: SeasonEnum;

  @Column()
  color: ColorEnum;

  @Column()
  theme: ThemeEnum;

  @Column()
  location: string;

  @Column()
  photo_video: string;

  @JoinColumn()
  @OneToOne((type) => UserEntity, (user) => user.weddingOptions)
  user: UserEntity;

  @Column()
  userId: number;
}
