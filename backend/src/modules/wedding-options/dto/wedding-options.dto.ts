import { Exclude, Expose, Type } from 'class-transformer';
import { AbstractDto } from './../../shared/abstract.dto';
import { UserDto } from '../../user/dto/user.dto';

import { ColorEnum, SeasonEnum, ThemeEnum } from './wedding-options.enums';

@Exclude()
export class WeddingOptionsDto extends AbstractDto {
  @Expose()
  season: SeasonEnum;

  @Expose()
  color: ColorEnum;

  @Expose()
  theme: ThemeEnum;

  @Expose()
  location: string;

  @Expose()
  photo_video: string;

  @Type(() => UserDto)
  user: UserDto;
}
