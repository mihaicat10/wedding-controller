import { IsIn, IsOptional } from 'class-validator';
import { ColorEnum, SeasonEnum, ThemeEnum } from './wedding-options.enums';

export class CreateWeddingOptionsDto {
  @IsOptional()
  id?: number;
  @IsOptional()
  season: SeasonEnum;

  @IsOptional()
  color: ColorEnum;

  @IsOptional()
  theme: ThemeEnum;

  @IsOptional()
  location: string;

  @IsOptional()
  photo_video: string;

  @IsOptional()
  weddingDate: string;
}
