export enum SeasonEnum {
  WINTER,
  SPRING,
  SUMMER,
  AUTUMN,
}

export enum ColorEnum {
  RED,
  PINK,
  BLACK,
  BLUE,
}

export enum ThemeEnum {
  BEACH,
  ALPINE,
  COZY,
}
