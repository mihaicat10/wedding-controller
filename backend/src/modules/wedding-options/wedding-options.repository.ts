import {
  ConflictException,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { EntityRepository, Repository } from 'typeorm';
import { UserEntity } from '../user/user.entity';
import { CreateWeddingOptionsDto } from './dto/create-wedding-options.dto';
import { WeddingOptionsDto } from './dto/wedding-options.dto';
import { WeddingOptionsEntity } from './wedding-options.entity';

@EntityRepository(WeddingOptionsEntity)
export class WeddingOptionsRepository extends Repository<WeddingOptionsEntity> {
  private logger = new Logger('WeddingOptionsRepository');

  async getWeddingOptions(user: UserEntity): Promise<WeddingOptionsDto> {
    const query = this.createQueryBuilder('weddingoptions');

    try {
      const weddingOptions = await query
        .leftJoinAndSelect('weddingoptions.user', 'user')
        .getOne()
        .then((res) => console.log(res))
        .catch((e) => console.log(e));

      return plainToClass(WeddingOptionsDto, weddingOptions);
      // return plainToClass(ChecklistDto, checklists);
    } catch (e) {
      this.logger.error(e.message, e.stack);
      throw new InternalServerErrorException();
    }
  }

  async createWeddingOptions(
    createWeddingOptions: CreateWeddingOptionsDto,
  ): Promise<WeddingOptionsDto> {
    const {
      color,
      location,
      photo_video,
      season,
      theme,
    } = createWeddingOptions;

    const existingWeddingOptions = await this.findOne(createWeddingOptions.id);

    if (!existingWeddingOptions) {
      const weddingOptionsEntity = new CreateWeddingOptionsDto();
      weddingOptionsEntity.color = color;
      weddingOptionsEntity.location = location;
      weddingOptionsEntity.photo_video = photo_video;
      weddingOptionsEntity.season = season;
      weddingOptionsEntity.theme = theme;

      return this.saveEntity(weddingOptionsEntity);
    }
    existingWeddingOptions.color = color;
    existingWeddingOptions.location = location;
    existingWeddingOptions.photo_video = photo_video;
    existingWeddingOptions.season = season;
    existingWeddingOptions.theme = theme;
    return this.saveEntity(existingWeddingOptions);
  }

  async saveEntity(entity): Promise<WeddingOptionsDto> {
    try {
      const result = await this.save(entity);
      return plainToClass(WeddingOptionsDto, result);
    } catch (e) {
      this.logger.error(e.message, e.stack);
      throw new InternalServerErrorException();
    }
  }
}
