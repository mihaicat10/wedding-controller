import {
  Body,
  Controller,
  Get,
  Logger,
  Post,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { handleUserRequest } from './../../common/common.service';
import { UserEntity } from '../user/user.entity';
import { CreateWeddingOptionsDto } from './dto/create-wedding-options.dto';
import { WeddingOptionsDto } from './dto/wedding-options.dto';
import { WeddingOptionsService } from './wedding-options.service';

@Controller('weddingoptions')
@UseGuards(AuthGuard('jwt'))
export class WeddingOptionsController {
  private logger = new Logger('WeddingOptionsController');
  // http://localhost:3001/weddingoptions
  constructor(private weddingOptionsService: WeddingOptionsService) {}

  @Get()
  getWeddingOptions(@Req() request): Promise<WeddingOptionsDto> {
    const user: UserEntity = handleUserRequest(request);
    this.logger.verbose(
      `User "${user.email}" is retrieving all wedding options`,
    );
    return this.weddingOptionsService.getWeddingOptions(user);
  }

  @Post()
  @UsePipes(ValidationPipe)
  createAndUpdateWeddingOptions(
    @Body() createWeddingDto: CreateWeddingOptionsDto,
    @Req() request,
  ): Promise<WeddingOptionsDto> {
    const user: UserEntity = handleUserRequest(request);
    this.logger.verbose(`User "${user.email}" is creating wedding options`);
    return this.weddingOptionsService.createWeddingOptions(
      createWeddingDto,
      user,
    );
  }
}
