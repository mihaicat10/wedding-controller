import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../user/user.entity';
import { CreateWeddingOptionsDto } from './dto/create-wedding-options.dto';
import { WeddingOptionsDto } from './dto/wedding-options.dto';
import { WeddingOptionsRepository } from './wedding-options.repository';

@Injectable()
export class WeddingOptionsService {
  private logger = new Logger('WeddingOptionsService');

  constructor(
    @InjectRepository(WeddingOptionsRepository)
    private weddingOptionsRepository: WeddingOptionsRepository,
  ) {}

  async getWeddingOptions(user: UserEntity): Promise<WeddingOptionsDto> {
    return this.weddingOptionsRepository.getWeddingOptions(user);
  }

  async createWeddingOptions(
    createWeddingOptionsDto: CreateWeddingOptionsDto,
    user: UserEntity,
  ): Promise<WeddingOptionsDto> {
    return this.weddingOptionsRepository.createWeddingOptions(
      createWeddingOptionsDto,
    );
  }
}
