import { IsOptional } from 'class-validator';
import { GuestStatusEnum } from './enums/guest-status-enum';
import { GuestTypeEnum } from './enums/guest-type.enum';

export class GetGuestFilterDto {
  @IsOptional()
  name: string;

  @IsOptional()
  type: GuestTypeEnum;

  @IsOptional()
  status: GuestStatusEnum;

  @IsOptional()
  limit: number;
}
