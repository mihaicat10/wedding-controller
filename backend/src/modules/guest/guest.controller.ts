import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { handleUserRequest } from '../../common/common.service';
import { UserEntity } from '../user/user.entity';
import { CreateGuestDto } from './dto/create-guest.dto';
import { GuestDto } from './dto/guest.dto';
import { GetGuestFilterDto } from './get-guest-filter.dto';
import { GuestService } from './guest.service';
import { UpdateGuestDto } from './dto/update-guest-dto';

@Controller('guest')
@UseGuards(AuthGuard('jwt'))
export class GuestController {
  private logger = new Logger('GuestController');

  constructor(private guestService: GuestService) {}

  @Get()
  getGuests(
    @Query(ValidationPipe) filterDto: GetGuestFilterDto,
    @Req() request,
  ): Promise<GuestDto[]> {
    const user: UserEntity = handleUserRequest(request);
    this.logger.verbose(
      `User "${user.email}" is retrieving all guests. Filters: ${JSON.stringify(
        filterDto,
      )}`,
    );
    return this.guestService.getGuests(filterDto, user);
  }

  @Get('/:id')
  getGuestById(
    @Param('id', ParseIntPipe) id: number,
    @Req() request,
  ): Promise<GuestDto> {
    const user: UserEntity = handleUserRequest(request);
    return this.guestService.getGuestById(id, user);
  }

  @Post()
  @UsePipes(ValidationPipe)
  createGuest(
    @Body() createGuestDto: CreateGuestDto,
    @Req() request,
  ): Promise<GuestDto> {
    const user: UserEntity = handleUserRequest(request);
    return this.guestService.createGuest(createGuestDto, user);
  }

  @Delete('/:id')
  deleteGuest(
    @Param('id', ParseIntPipe) id: number,
    @Req() request,
  ): Promise<any> {
    const user: UserEntity = handleUserRequest(request);
    this.logger.log(id);
    return this.guestService.deleteGuest(id, user);
  }

  @Patch()
  updateGuestStatus(
    @Body() updateGuestDto: UpdateGuestDto,
    @Req() request,
  ): Promise<GuestDto> {
    const user: UserEntity = handleUserRequest(request);
    return this.guestService.updateGuestStatus(updateGuestDto, user);
  }
}
