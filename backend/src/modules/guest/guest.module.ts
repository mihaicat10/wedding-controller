import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { UserRepository } from '../user/user.repository';
import { GuestController } from './guest.controller';
import { GuestRepository } from './guest.repository';
import { GuestService } from './guest.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([GuestRepository, UserRepository]),
    AuthModule,
  ],
  controllers: [GuestController],
  providers: [GuestService],
})
export class GuestModule {}
