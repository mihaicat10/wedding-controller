import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { UserEntity } from '../user/user.entity';
import { plainToClass } from 'class-transformer';
import { UserDto } from '../user/dto/user.dto';
import { GuestEntity } from './guest.entity';
import { GuestDto } from './dto/guest.dto';
import { GetGuestFilterDto } from './get-guest-filter.dto';
import { CreateGuestDto } from './dto/create-guest.dto';

@EntityRepository(GuestEntity)
export class GuestRepository extends Repository<GuestEntity> {
  private logger = new Logger('GuestRepository');

  async getGuests(
    filterDto: GetGuestFilterDto,
    user: UserEntity,
  ): Promise<GuestDto[]> {
    const { name, status, type, limit } = filterDto;
    const query = this.createQueryBuilder('guest');

    if (name) {
      query.andWhere('guest.name = :name', { name });
    }
    query.getMany();
    if (status) {
      query.andWhere('guest.status = :status', { status });
    }
    if (type) {
      query.andWhere('guest.type = :type', { type });
    }

    if (limit) {
      query.take(limit);
    }

    try {
      const guest = await query
        .leftJoinAndSelect('guest.user', 'user')
        .getMany();
      return plainToClass(GuestDto, guest);
    } catch (error) {
      this.logger.error(error.message, error.stack);
      throw new InternalServerErrorException();
    }
  }

  async createGuest(
    createGuestDto: CreateGuestDto,
    user: UserEntity,
  ): Promise<GuestDto> {
    const { type, name, status } = createGuestDto;

    const guest = new GuestEntity();
    guest.name = name;
    guest.type = type;
    guest.status = status;
    guest.user = user;

    try {
      const result = await this.save(guest);
      return plainToClass(GuestDto, result);
    } catch (error) {
      //   console.log('asta e eroarea: ', error);
      this.logger.error(error.message, error.stack);
      if (error.code === '23505') {
        throw new ConflictException(null, 'Guest already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  async getGuestById(id: number, user: UserDto): Promise<GuestDto> {
    try {
      const guest = await this.findOne(id);
      return plainToClass(GuestDto, guest);
    } catch (e) {
      throw new NotFoundException(`Guest with id: ${id} not found`);
    }
  }
}
