import { IsNotEmpty } from 'class-validator';
import { GuestStatusEnum } from '../enums/guest-status-enum';
import { GuestTypeEnum } from '../enums/guest-type.enum';

export class CreateGuestDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  type: GuestTypeEnum;

  @IsNotEmpty()
  status: GuestStatusEnum;
}
