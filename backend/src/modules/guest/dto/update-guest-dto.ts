import { IsNotEmpty, IsOptional } from 'class-validator';
import { GuestStatusEnum } from '../enums/guest-status-enum';
import { GuestTypeEnum } from '../enums/guest-type.enum';

export class UpdateGuestDto {
  @IsOptional()
  name: string;

  @IsOptional()
  type: GuestTypeEnum;

  @IsOptional()
  status: GuestStatusEnum;

  @IsNotEmpty()
  id: number;

  @IsOptional()
  createdAt: Date;
  @IsOptional()
  updatedAt: Date;
}
