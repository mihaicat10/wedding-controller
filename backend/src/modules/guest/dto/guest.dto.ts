import { Exclude, Expose, Type } from 'class-transformer';
import { AbstractDto } from '../../shared/abstract.dto';

import { UserDto } from '../../user/dto/user.dto';
import { GuestStatusEnum } from '../enums/guest-status-enum';
import { GuestTypeEnum } from '../enums/guest-type.enum';

@Exclude()
export class GuestDto extends AbstractDto {
  @Expose()
  name: string;

  @Expose()
  type: GuestTypeEnum;

  @Expose()
  status: GuestStatusEnum;

  @Type(() => UserDto)
  user: UserDto;
}
