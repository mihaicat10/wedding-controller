export enum GuestStatusEnum {
  PENDING,
  DECLINED,
  ACCEPTED,
}
