export enum GuestTypeEnum {
  GODFATHER,
  GODMOTHER,
  GUEST_OF_HONOR,
  MAID_OF_HONOR,
  SIMPLE,
}
