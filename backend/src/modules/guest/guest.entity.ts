import { Column, Entity, ManyToOne, Unique } from 'typeorm';
import { AbstractEntity } from '../shared/abstract.entity';
import { UserEntity } from '../user/user.entity';
import { GuestStatusEnum } from './enums/guest-status-enum';
import { GuestTypeEnum } from './enums/guest-type.enum';

@Entity({ name: 'guest' })
@Unique(['name'])
export class GuestEntity extends AbstractEntity {
  @Column()
  name: string;

  @Column({
    type: 'enum',
    enum: GuestTypeEnum,
    default: GuestTypeEnum.SIMPLE,
  })
  type: GuestTypeEnum;

  @Column({
    type: 'enum',
    enum: GuestStatusEnum,
    default: GuestStatusEnum.PENDING,
  })
  status: GuestStatusEnum;

  @ManyToOne((type) => UserEntity, (user) => user.guest)
  user: UserEntity;

  @Column()
  userId: number;
}
