import {
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { UserEntity } from '../user/user.entity';
import { CreateGuestDto } from './dto/create-guest.dto';
import { GuestDto } from './dto/guest.dto';
import { UpdateGuestDto } from './dto/update-guest-dto';
import { GetGuestFilterDto } from './get-guest-filter.dto';
import { GuestRepository } from './guest.repository';

@Injectable()
export class GuestService {
  private logger = new Logger('GuestService');

  constructor(
    @InjectRepository(GuestRepository)
    private guestRepository: GuestRepository,
  ) {}

  async getGuests(
    filterDto: GetGuestFilterDto,
    user: UserEntity,
  ): Promise<GuestDto[]> {
    return this.guestRepository.getGuests(filterDto, user);
  }

  async getGuestById(id: number, user: UserEntity): Promise<GuestDto> {
    return this.guestRepository.getGuestById(id, user);
  }

  async createGuest(
    createGuestDto: CreateGuestDto,
    user: UserEntity,
  ): Promise<GuestDto> {
    return this.guestRepository.createGuest(createGuestDto, user);
  }

  async deleteGuest(id: number, user: UserEntity): Promise<any> {
    const result = await this.guestRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(`Guest with ID "${id}" not found`);
    }

    return Promise.resolve({
      result: result,
      status: 'success',
      id,
    });
  }

  async updateGuestStatus(
    updateDto: UpdateGuestDto,
    user: UserEntity,
  ): Promise<GuestDto> {
    const guest = await this.getGuestById(Number(updateDto.id), user);
    if (!guest) {
      this.logger.error('Guest was not found');
      throw new NotFoundException();
    }
    guest.name = updateDto.name;
    guest.type = updateDto.type;
    guest.status = updateDto.status;
    guest.updatedAt = new Date();

    try {
      const result = await this.guestRepository.save(guest);
      return plainToClass(GuestDto, result);
    } catch (e) {
      throw new InternalServerErrorException({
        message: 'Cannot update guest',
      });
    }
  }
}
