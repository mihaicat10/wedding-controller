import { ChecklistEntity } from './checklist.entity';
import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { GetChecklistFilterDto } from './dto/get-checklist-filter.dto';
import { UserEntity } from '../user/user.entity';
import { ChecklistDto } from './dto/checklist.dto';
import { plainToClass } from 'class-transformer';
import { CreateChecklistDto } from './dto/create-checklist.dto';
import { UserDto } from '../user/dto/user.dto';

@EntityRepository(ChecklistEntity)
export class ChecklistRepository extends Repository<ChecklistEntity> {
  private logger = new Logger('ChecklistRepository');

  async getChecklists(
    filterDto: GetChecklistFilterDto,
    user: UserEntity,
  ): Promise<ChecklistDto[]> {
    const { status, search } = filterDto;
    const query = this.createQueryBuilder('checklist');

    if (status) {
      query.andWhere('checklist.status = :status', { status });
    }

    if (search) {
      query.andWhere(
        '(checklist.title LIKE :search OR checklist.description LIKE :search)',
        { search: `%${search}%` },
      );
    }

    try {
      const checklists = await query
        .leftJoinAndSelect('checklist.user', 'user')
        .getMany();
      return plainToClass(ChecklistDto, checklists);
    } catch (error) {
      this.logger.error(error.message, error.stack);
      throw new InternalServerErrorException();
    }
  }

  async createChecklist(
    createChecklistDto: CreateChecklistDto,
    user: UserEntity,
  ): Promise<ChecklistDto> {
    const { title, description } = createChecklistDto;

    const checklist = new ChecklistEntity();
    checklist.title = title;
    checklist.description = description;
    checklist.user = user;

    try {
      const result = await this.save(checklist);
      return plainToClass(ChecklistDto, result);
    } catch (error) {
      //   console.log('asta e eroarea: ', error);
      this.logger.error(error.message, error.stack);
      if (error.code === '23505') {
        // duplicate checklist
        throw new ConflictException(null, 'Checklist already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  async getChecklistById(id: number, user: UserDto): Promise<ChecklistDto> {
    try {
      const checklist = await this.findOne(id); // find by id
      return plainToClass(ChecklistDto, checklist);
    } catch (e) {
      throw new NotFoundException(`Checklist with id: ${id} not found`);
    }
  }
}

// const values = this.billRepository.createQueryBuilder("bill")
//     .leftJoinAndSelect("bill.user", "user")
//     .where("bill.accountBill LIKE :accountBill", {accountBill})
//     .andWhere("user.id = :userId", {userId: user.id})
//     .select(["user.name", "user.surname"])
//     .execute();
