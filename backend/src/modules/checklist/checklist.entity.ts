import {Entity, Column, ManyToOne, Unique} from 'typeorm';
import {UserEntity} from '../user/user.entity';
import {AbstractEntity} from '../shared/abstract.entity';
import {ChecklistStatus} from './checklist-status.enum';

@Entity({name: 'checklist'})
@Unique(['title'])
export class ChecklistEntity extends AbstractEntity {
    @Column()
    title: string;

    @Column()
    description: string;

    @Column({
        type: 'enum',
        enum: ChecklistStatus,
        default: ChecklistStatus.OPEN,
    })
    status: ChecklistStatus;

    @ManyToOne((type) => UserEntity, (user) => user.checklist)
    user: UserEntity;

    @Column()
    userId: number;
}

