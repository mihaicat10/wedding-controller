import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { ChecklistRepository } from './checklist.repository';
import { ChecklistController } from './checklist.controller';
import { ChecklistService } from './checklist.service';
import { AuthModule } from '../auth/auth.module';
import { UserRepository } from '../user/user.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([ChecklistRepository, UserRepository]),
    AuthModule,
  ],
  controllers: [ChecklistController],
  providers: [ChecklistService],
})
export class ChecklistModule {}
