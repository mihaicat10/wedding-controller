import { BadRequestException, PipeTransform } from '@nestjs/common';
import { ChecklistStatus } from '../checklist-status.enum';

export class ChecklistStatusValidationPipe implements PipeTransform {
  readonly allowedStatuses = [
    ChecklistStatus.OPEN,
    ChecklistStatus.IN_PROGRESS,
    ChecklistStatus.DONE,
  ];

  transform(value: any): any {
    value = value.toUpperCase();

    if (!this.isStatusValid(value)) {
      throw new BadRequestException(`"${value}" is an invalid status`);
    }

    return value;
  }

  private isStatusValid(status: any) {
    const idx = this.allowedStatuses.indexOf(status);
    return idx !== -1;
  }
}
