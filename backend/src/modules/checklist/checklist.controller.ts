import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ChecklistService } from './checklist.service';
import { UserEntity } from '../user/user.entity';
import { GetChecklistFilterDto } from './dto/get-checklist-filter.dto';
import { ChecklistDto } from './dto/checklist.dto';
import { CreateChecklistDto } from './dto/create-checklist.dto';
import { ChecklistStatusValidationPipe } from './pipes/checklist-status-validation.pipe';
import { ChecklistStatus } from './checklist-status.enum';
import { handleUserRequest } from '../../common/common.service';

@Controller('checklist')
@UseGuards(AuthGuard('jwt'))
export class ChecklistController {
  private logger = new Logger('ChecklistController');

  constructor(private checklistService: ChecklistService) {}

  @Get()
  getChecklists(
    @Query(ValidationPipe) filterDto: GetChecklistFilterDto,
    @Req() request,
  ): Promise<ChecklistDto[]> {
    const user: UserEntity = handleUserRequest(request);
    this.logger.verbose(
      `User "${
        user.email
      }" is retrieving all checklists. Filters: ${JSON.stringify(filterDto)}`,
    );
    return this.checklistService.getChecklists(filterDto, user);
  }

  @Get('/:id')
  getChecklistById(
    @Param('id', ParseIntPipe) id: number,
    @Req() request,
  ): Promise<ChecklistDto> {
    const user: UserEntity = handleUserRequest(request);

    return this.checklistService.getChecklistById(id, user);
  }

  @Post()
  @UsePipes(ValidationPipe)
  createChecklist(
    @Body() createChecklistDto: CreateChecklistDto,
    @Req() request,
  ): Promise<ChecklistDto> {
    const user: UserEntity = handleUserRequest(request);
    return this.checklistService.createChecklist(createChecklistDto, user);
  }

  @Delete('/:id')
  deleteChecklist(
    @Param('id', ParseIntPipe) id: number,
    @Req() request,
  ): Promise<any> {
    const user: UserEntity = handleUserRequest(request);
    this.logger.log(id);
    return this.checklistService.deleteChecklist(id, user);
  }

  @Patch('/:id/status')
  updateChecklistStatus(
    @Param('id', ParseIntPipe) id: number,
    @Body('status', ChecklistStatusValidationPipe) status: ChecklistStatus,
    @Req() request,
  ): Promise<ChecklistDto> {
    const user: UserEntity = handleUserRequest(request);
    return this.checklistService.updateChecklistStatus(id, status, user);
  }
}
