import {
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ChecklistRepository } from './checklist.repository';
import { GetChecklistFilterDto } from './dto/get-checklist-filter.dto';
import { UserEntity } from '../user/user.entity';
import { ChecklistDto } from './dto/checklist.dto';
import { plainToClass } from 'class-transformer';
import { CreateChecklistDto } from './dto/create-checklist.dto';
import { ChecklistStatus } from './checklist-status.enum';

@Injectable()
export class ChecklistService {
  private logger = new Logger('ChecklistService');

  constructor(
    @InjectRepository(ChecklistRepository)
    private checklistRepository: ChecklistRepository,
  ) {}

  async getChecklists(
    filterDto: GetChecklistFilterDto,
    user: UserEntity,
  ): Promise<ChecklistDto[]> {
    return this.checklistRepository.getChecklists(filterDto, user);
  }

  async getChecklistById(id: number, user: UserEntity): Promise<ChecklistDto> {
    return this.checklistRepository.getChecklistById(id, user);
  }

  async createChecklist(
    createChecklistDto: CreateChecklistDto,
    user: UserEntity,
  ): Promise<ChecklistDto> {
    return this.checklistRepository.createChecklist(createChecklistDto, user);
  }

  async deleteChecklist(id: number, user: UserEntity): Promise<any> {
    // Maybe a soft delete instead?
    const result = await this.checklistRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(`Checklist with ID "${id}" not found`);
    }

    return Promise.resolve({
      result: result,
      status: 'succes',
      id,
    });
  }

  async updateChecklistStatus(
    id: number,
    status: ChecklistStatus,
    user: UserEntity,
  ): Promise<ChecklistDto> {
    const checklist = await this.getChecklistById(id, user);
    if (!checklist) {
      this.logger.error('Checklist was not found');
      throw new NotFoundException();
    }
    checklist.status = status;
    checklist.updatedAt = new Date();

    try {
      const result = await this.checklistRepository.save(checklist);
      return plainToClass(ChecklistDto, result);
    } catch (e) {
      throw new InternalServerErrorException({
        messsage: 'Cannot update checklist',
      });
    }
  }
}
