import { AbstractDto } from '../../shared/abstract.dto';
import { Exclude, Expose, Type } from 'class-transformer';
import { ChecklistStatus } from '../checklist-status.enum';
import { UserDto } from '../../user/dto/user.dto';

@Exclude()
export class ChecklistDto extends AbstractDto {
  @Expose()
  title: string;

  @Expose()
  description: string;

  @Expose()
  status: ChecklistStatus;

  @Expose()
  userId: number;

  @Type(() => UserDto)
  user: UserDto;
}
