import {IsIn, IsNotEmpty, isNotEmpty, IsOptional} from "class-validator";
import {ChecklistStatus} from "../checklist-status.enum";

export class GetChecklistFilterDto {
    @IsOptional()
    @IsIn([ChecklistStatus.OPEN, ChecklistStatus.IN_PROGRESS, ChecklistStatus.DONE])
    status: ChecklistStatus;

    @IsOptional()
    // @IsNotEmpty()
    search: string;
}