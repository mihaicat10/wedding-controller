import { IsNotEmpty, IsOptional } from 'class-validator';
import { ChecklistStatus } from '../checklist-status.enum';

export class CreateChecklistDto {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  description: string;

  @IsOptional()
  status: ChecklistStatus;
}
