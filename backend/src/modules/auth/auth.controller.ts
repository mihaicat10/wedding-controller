import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { LoginCredentialsDto } from './dto/login-credentials.dto';
import { RegisterCredentialsDto } from './dto/register-credentials.dto';
import { handleUserRequest } from '../../common/common.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  signUp(
    @Body(ValidationPipe) registerCredentialsDto: RegisterCredentialsDto,
  ): Promise<void> {
    return this.authService.signUp(registerCredentialsDto);
  }

  @Post('/signin')
  signIn(
    @Body(ValidationPipe) loginCredentialsDto: LoginCredentialsDto,
  ): Promise<{ accessToken: string }> {
    return this.authService.signIn(loginCredentialsDto);
  }

  @Get('/authenticated')
  @UseGuards(AuthGuard('jwt'))
  getAuthenticatedUser(@Req() request): AuthCredentialsDto {
    const user = handleUserRequest(request);
    return this.authService.getAuthenticatedUser(user);
  }
}
