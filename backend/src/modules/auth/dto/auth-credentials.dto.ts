import {
  IsDefined,
  IsEmail,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class AuthCredentialsDto {
  @IsDefined()
  @MinLength(4)
  @MaxLength(30)
  @IsEmail()
  readonly email: string;

  @IsDefined()
  @IsString()
  @MinLength(8)
  // @Matches(
  //   /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
  //   { message: 'password too weak' },
  // )
  readonly password: string;
}
