import { IsString, MinLength, MaxLength, IsDefined } from 'class-validator';

// TODO: Implement full registerDto
export class RegisterCredentialsDto {
  @IsDefined()
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  readonly email: string;

  @IsDefined()
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  readonly password: string;

  // @IsString()
  // @IsOptional()
  // readonly lastName: string;
  //
  // @IsDefined()
  // @IsString()
  // @MinLength(8)
  // @MaxLength(20)
  // @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
  //   message: 'password too weak',
  // })
  // readonly password: string;
  //
  // @IsPhoneNumber('ZZ')
  // @IsOptional()
  // phone: string;
}
