import {
  IsDefined,
  IsEmail,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class LoginCredentialsDto {
  @IsDefined()
  @MinLength(4)
  @MaxLength(254)
  @IsEmail()
  email: string;

  @IsDefined()
  @IsString()
  @MinLength(8)
  // TODO: Add this in production
  // @Matches(
  //   /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
  //   { message: 'password too weak' },
  // )
  password: string;
}
