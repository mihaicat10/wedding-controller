import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtPayloadInterface } from './jwt-payload.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '../user/user.repository';
import { UserEntity } from '../user/user.entity';
import * as config from 'config';
import {getConnection} from "typeorm";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET || config.get('jwt.secret'),
      ignoreExpiration: false,
    });
  }

  async validate(payload: JwtPayloadInterface): Promise<UserEntity> {
      const user = await getConnection()
          .createQueryBuilder()
          .select("user")
          .from(UserEntity, "user")
          .where("user.email = :email", { email: payload.email })
          .getOne();

    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
