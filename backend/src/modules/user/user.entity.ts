import { AbstractEntity } from '../shared/abstract.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  Unique,
} from 'typeorm';
import { UserRole } from './user-role-enum';
import { ChecklistEntity } from '../checklist/checklist.entity';
import * as bcrypt from 'bcrypt';
import { Budget } from '../budget/budget.entity';
import { GuestEntity } from '../guest/guest.entity';
import { WeddingOptionsEntity } from '../wedding-options/wedding-options.entity';
import {
  SeasonEnum,
  ColorEnum,
  ThemeEnum,
} from '../../../../frontend/src/redux/IState';

@Entity({ name: 'user' })
@Unique(['email'])
export class UserEntity extends AbstractEntity {
  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  // @Column()
  // weddingDate: string;

  // @Column()
  // partnerName: string;

  // season: SeasonEnum;

  // @Column()
  // color: ColorEnum;

  // @Column()
  // theme: ThemeEnum;

  // @Column()
  // location: string;

  // @Column()
  // photo_video: string;

  // @Column({
  //   type: 'enum',
  //   enum: GuestTypeEnum,
  //   default: GuestTypeEnum.SIMPLE,
  // })
  // type: GuestTypeEnum;
  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.USER,
  })
  role;

  @OneToMany((type) => ChecklistEntity, (checklist) => checklist.user, {
    eager: true,
    // cascade: true,
  })
  checklist: ChecklistEntity[];

  @OneToOne((type) => Budget, (budget) => budget.user, {
    eager: true,
  })
  budget: Budget;

  @OneToMany(() => GuestEntity, (guest) => guest.user, {
    eager: true,
  })
  guest: GuestEntity[];

  @OneToOne(
    () => WeddingOptionsEntity,
    (weddingoptions) => weddingoptions.user,
    {
      eager: true,
    },
  )
  weddingOptions: WeddingOptionsEntity[];

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }
}
