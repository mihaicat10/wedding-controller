import { Exclude, Expose, Type } from 'class-transformer';
import { ColorEnum } from 'src/modules/wedding-options/dto/wedding-options.enums';
import { AbstractDto } from '../../shared/abstract.dto';
import { UserRole } from '../user-role-enum';
import {
  SeasonEnum,
  ThemeEnum,
} from '../../../../../frontend/src/redux/IState';

@Exclude()
export class UserDto extends AbstractDto {
  @Expose()
  email: string;

  @Exclude()
  password: string;

  @Exclude()
  salt: string;

  // @Expose()
  // weddingDate: string;

  // @Expose()
  // partnerName: string;

  // season: SeasonEnum;

  // @Expose()
  // color: ColorEnum;

  // @Expose()
  // location: string;

  // @Expose()
  // photo_video: string;

  // @Expose()
  // theme: ThemeEnum;

  @Expose()
  role: UserRole;
}
