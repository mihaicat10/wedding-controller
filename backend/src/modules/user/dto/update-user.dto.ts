import {
  IsString,
  IsPhoneNumber,
  IsOptional,
  IsIn,
  MaxLength,
} from 'class-validator';
import { UserRole } from '../user-role-enum';

export class UpdateUserDto {
  @IsString()
  @IsOptional()
  @MaxLength(50)
  readonly myName: string;

  @IsString()
  @IsOptional()
  @MaxLength(50)
  readonly lastName: string;

  @IsPhoneNumber('ZZ')
  @IsOptional()
  phone: string;

  @IsOptional()
  @IsIn([UserRole.ADMIN, UserRole.VENDOR, UserRole.USER, UserRole.SU_ADMIN])
  role: UserRole;
}
