import {
  IsDate,
  IsEnum,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';
import {
  SeasonEnum,
  ColorEnum,
  ThemeEnum,
} from '../../../../../frontend/src/redux/IState';
export class UserPreferenceDto {
  @IsOptional()
  id?: string;
  @IsOptional()
  @IsDate()
  weddingDate: string;
  @IsString()
  @IsOptional()
  @MaxLength(255)
  partnerName: string;

  @IsEnum(SeasonEnum)
  @IsOptional()
  season: SeasonEnum;

  @IsEnum(ColorEnum)
  @IsOptional()
  color: ColorEnum;

  @IsEnum(ThemeEnum)
  @IsOptional()
  theme: ThemeEnum;

  @IsString()
  @MaxLength(255)
  @IsOptional()
  location: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  photo_video: string;
}
