export enum UserRole {
  ADMIN = 'ADMIN',
  USER = 'USER',
  VENDOR = 'VENDOR',
  SU_ADMIN = 'SU_ADMIN',
}
