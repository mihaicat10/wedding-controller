import { Repository, EntityRepository } from 'typeorm';

import {
  InternalServerErrorException,
  Logger,
  ConflictException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { plainToClass } from 'class-transformer';
import { UserEntity } from './user.entity';
import { UserDto } from './dto/user.dto';
import { CreateUserDto } from './dto/create-user.dto';

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {
  private logger = new Logger('UserRepository');

  // TODO: update createUserDto & this repository
  async createUser(
    createUserDto: CreateUserDto,
    user: UserEntity,
  ): Promise<UserDto> {
    const { email, password, role } = createUserDto;

    const userEntity = new UserEntity();
    userEntity.salt = await bcrypt.genSalt();
    userEntity.password = await bcrypt.hash(password, user.salt);
    userEntity.role = role;

    try {
      const createdUser = await this.save(userEntity);
      return plainToClass(UserDto, createdUser);
    } catch (error) {
      this.logger.error(error.message, error.stack);
      if (error.code === '23505') {
        // duplicate username
        throw new ConflictException('Username already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }
}
