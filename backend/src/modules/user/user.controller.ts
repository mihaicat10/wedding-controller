import {
  Controller,
  Get,
  UseGuards,
  Param,
  ParseIntPipe,
  Delete,
  Post,
  ValidationPipe,
  Body,
  Logger,
  UsePipes,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { handleUserRequest } from '../../common/common.service';
import { UserPreferenceDto } from './dto/user-preference.dto';

@Controller('users')
@UseGuards(AuthGuard())
export class UserController {
  private logger = new Logger('UserController');

  constructor(private userService: UserService) {}

  @Get()
  getUsers(
    @Query('page') page = 1,
    @Query('limit') limit = 10,
  ): Promise<UserDto[]> {
    return this.userService.getUsers({
      page,
      limit,
    });
  }

  @Get('/:id')
  getUser(@Param('id', ParseIntPipe) id: number, @Req() req): Promise<UserDto> {
    const user = handleUserRequest(req);
    return this.userService.getUserById(id, user);
  }

  // @Post('/user-preference')
  // @UsePipes(ValidationPipe)
  // modifyUserPreferences(
  //   @Body() userPreferenceDto: UserPreferenceDto,
  //   @Req() req,
  // ): Promise<UserPreferenceDto> {
  //   const user = handleUserRequest(req);
  //   return this.userService.modifyUserPreference(userPreferenceDto, user);
  // }

  @Post()
  @UsePipes(ValidationPipe)
  createUser(
    @Body() createUserDto: CreateUserDto,
    @Req() req,
  ): Promise<UserDto> {
    const user = handleUserRequest(req);
    return this.userService.createUser(createUserDto, user);
  }

  @Delete('/:id')
  deleteUser(@Param('id', ParseIntPipe) id: number, @Req() req): Promise<any> {
    const user = handleUserRequest(req);
    return this.userService.deleteUser(id, user);
  }

  @Put('/:id')
  @UsePipes(ValidationPipe)
  updateUser(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateUserDto: UpdateUserDto,
    @Req() req,
  ): Promise<UserDto> {
    const user = handleUserRequest(req);
    return this.userService.updateUser(id, updateUserDto, user);
  }
}
