import {TypeOrmModule} from "@nestjs/typeorm";
import {UserRepository} from "./user.repository";
import {AuthModule} from "../auth/auth.module";
import {UserController} from "./user.controller";
import {UserService} from "./user.service";
import {Module} from "@nestjs/common";

@Module({
    imports: [
        // forwardRef(() => AuthModule), it works like importing AuthModule
        TypeOrmModule.forFeature([UserRepository]),
        AuthModule
    ],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule {
}
