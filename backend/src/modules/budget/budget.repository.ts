import { Logger } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { Budget } from './budget.entity';

@EntityRepository(Budget)
export class BudgetRepository extends Repository<Budget> {
  private logger = new Logger('BudgetRepository');
  
}
