import {Entity, Column, ManyToOne, Unique} from 'typeorm';
import {UserEntity} from '../user/user.entity';
import {AbstractEntity} from '../shared/abstract.entity';

@Entity({name: 'budget'})
export class Budget extends AbstractEntity {
    // TODO: add in future more data
    @Column()
    total: string;

    @Column()
    left: string;
    
    @ManyToOne((type) => UserEntity, (user) => user.budget)
    user: UserEntity;

    @Column()
    userId: number;
}

