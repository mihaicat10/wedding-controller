import {TypeOrmModule} from '@nestjs/typeorm';
import {Module} from '@nestjs/common';
import {AuthModule} from '../auth/auth.module';
import {UserRepository} from "../user/user.repository";

@Module({
    imports: [TypeOrmModule.forFeature([UserRepository]), AuthModule],
    controllers: [],
    providers: [],
})
export class ChecklistModule {
}
